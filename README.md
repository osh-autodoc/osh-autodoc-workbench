<!--
SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>

SPDX-License-Identifier: LGPL-3.0-or-later
-->

# FreeCAD OSH Automated Documentation Workbench (OSH AutoDoc Workbench)

## Introduction

This workbench aims to support the creation of assembly manuals of open source
hardware. It is mainly focused on creating an abstraction of assembly steps in
an instruction manual, generating BOMs, capturing camera positions, and angles.

## Installation

- Clone our freecad source fork and checkout the `automated-documentation` branch.

- Follow the official FreeCAD documentation to Compile FreeCAD on your specific GNU/Linux distribution.

- Clone this repository

- Create a symlink of this workbench into the Mod Folder

## Accessing the workbench in the FreeCAD console

To access the workbench in the FreeCAD console, type the following in the console:

```python
import freecad.OSHAutoDocWorkbench as autodoc
```

You can now access the attributes:

```python
autodoc.StepSelect.panel
```

Similarly, to access other attributes, such as BOM functions:

```python
import freecad.OSHAutoDocWorkbench.bom.bom_tools as bom
```

You may access methods, for example:

```python
bom.getDirVec()
```

## Assembly manual design methodology

- Import Design
- Inspect Design
- Select Parts
- Position Parts
- Define Steps
- Set Camera positions
- Generate Images and Bill of Materials (BOM)

## Test OSH-Autodoc

- Live image for OSH-Autodoc: https://zenodo.org/record/7633515

- OSH-Autodoc apptainer: https://zenodo.org/record/7652868

## Video

![Vise Demo Video Clip](res/asset/media/vid/vise-video-demo.gif)


- [Overview video](https://zenodo.org/record/7633581)
- [Vise video](https://zenodo.org/record/7633593)

## Citation

```bibtex
@software{Mariscal-Melgar2023FreeCAD,
  author       = {Mariscal-Melgar, J.C. and
                  Hijma, Pieter},
  title        = {FreeCAD OSH Automated Documentation Workbench},
  month        = feb,
  year         = 2023,
  note         = {{This research was funded by the European Regional
                   Development Fund (ERDF) in the context of the
                   INTERFACER Project.}},
  publisher    = {Zenodo},
  version      = {v0.1.0},
  doi          = {10.5281/zenodo.7633440},
  url          = {https://doi.org/10.5281/zenodo.7633440}
}
```

## License

Most of the project is licensed under LGPL 3.0 or later.  See the LICENSES
folder and SPDX metadata for more dteails.  The project is
[REUSE](https://reuse.software/) compliant.

## Authors

- Pieter Hijma
- J.C. Mariscal-Melgar

These authors contributed equally.

## Funding

This project has been funded by the European Regional Development Fund (ERDF)
in the context of the [INTERFACER Project](https://www.interfacerproject.eu/)
from October 2021 until March 2023.

![Logo of the EU ERDF program](https://cloud.fabcity.hamburg/s/TopenKEHkWJ8j5P/download/logo-eu-erdf.png)

This project was a research project and an article about this work is currently
under review.  However, the software has attracted much interest from the Open
Source Hardware community and we are actively searching for funding to
transform the software from a research prototype to a tool that is easy to
install and use ([#11](https://codeberg.org/osh-autodoc/osh-autodoc-workbench/issues/11)).

