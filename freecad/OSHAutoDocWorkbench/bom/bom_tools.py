# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
from pathlib import Path
import re

import FreeCAD as App
import FreeCADGui as Gui
import PySide.QtGui
import pivy.coin as coin

from ..util.debug import logDebug
from ..util.debug import d
from ..util.debug import logMessage
from ..util.util import renameFileName

from ..svg.svg import getSVG
import freecad.OSHAutoDocWorkbench.bom.camera as camera

HOME_PATH = str(Path.home())
TEMPLATE_FILE_PATH = os.path.join(os.path.split(os.path.dirname(__file__))[0],
                                  'resources',
                                  'td_template_blank.svg')
DEFAULT_SAVE_PATH = os.path.join(HOME_PATH, 'tmp', 'tmp-freecad')
# default OS agnostic freecad application path
USER_APP_PATH = App.ConfigGet("UserAppData")
USER_APP_PATH_TEMP = os.path.join(USER_APP_PATH, 'tmp')

# =====================================
# FreeCAD Object Functions
# =====================================


def getObjChildrenProperties(obj, prop):
    """Return list of children properties of any group objects

    obj, str -> list

    obj: object
    prop: property to get

    Note: prop cannot be group or links
    TODO: add functionality to handle component props
    """
    propertyList = []

    if 'Links' in obj.PropertiesList:
        for child in obj.Links:
            if prop in child.PropertiesList:
                propertyList.append(getattr(child, prop))
            else:
                propertyList.append("NA")

    elif 'Group' in obj.PropertiesList:
        for child in obj.Group:
            if prop in child.PropertiesList:
                propertyList.append(getattr(child, prop))
            else:
                propertyList.append("NA")
    else:
        logMessage("no property found", 'w')

    return propertyList

def getObjChildPropRec(obj, prop, derivedFrom):
    """returns property list of children of a group object recursively
    getObjChildPropRec(obj, 'Label', 'Part::Feature')
    """
    objs = getObjChildrenRecursively(obj)
    propertyList = []
    for child in objs:
        if prop in child.PropertiesList:
            propertyList.append(getattr(child,prop))
        else:
            propertyList.append("NA")
    return propertyList


def getObjChildren(obj):
    """Return list of children of obj

    obj -> list
    """
    if 'Links' in obj.PropertiesList:
        return obj.Links
    elif 'Group' in obj.PropertiesList:
        return obj.Group
    else:
        logMessage("no children found", 'w')


def getObjChildrenRecursively(obj):
    """Return a list of object children
    eg.
    getObjChildren(obj)
    """
    objs = []
    if hasattr(obj, 'Group'):
        for i in obj.Group:
            if not (hasattr(i, 'Group')):
                objs.append(i)
            else:
                if i is not None:
                    objs.extend(getObjChildrenRecursively(i))
        return objs

# TODO: test
def getObjChildrenDerivedFrom(obj, derivedFrom):
    """Return a list of object children derived from a specific class
    eg.
    getObjChildrenDerivedFrom(obj, 'Part::Feature')
    """
    objs = []
    if hasattr(obj, 'Group'):
        for i in obj.Group:
            if i.isDerivedFrom(derivedFrom):
                objs.append(i)
            elif hasattr(i, 'Group'):
                objs.extend(getObjChildrenDerivedFrom(i, derivedFrom))
        return objs


def getObjOutList(obj, derivedFrom):
    """Return a Outlist of object children derived from a specific class
    eg.
    getObjOutList(obj, 'Part::Feature')
    """
    objs = [ ]
    for i in obj.OutListRecursive:
        if i.isDerivedFrom(derivedFrom):
            objs.append(i)
    return objs

def createGroupObjChildrenDic(groupObj):
    """Creates a 'object child': 'number of ocurrences' dictionary of group

    obj -> Dictionary

    Example:

    >> createGroupObjChildrenDic(App.ActiveDocument.getObject("Layer001"))
    {'Profile 30-350': 2, 'Profile 30-380': 2}

    Note:
    - handles:
      - Layer object
      - TODO: group, component, part

    """
    # old method
    # objLabels = getObjChildrenProperties(groupObj, 'Label')
    objLabels = getObjChildPropRec(groupObj, 'Label', 'Part::Feature')
    patterns = getPatternsFromLabels(objLabels)
    groupObjDic = countLabels(objLabels, patterns)
    return groupObjDic

# =====================================
# Export functions
# =====================================

# TODO: fix colours of lines
def exportSvgFromLSC(layerStateContainer, path=USER_APP_PATH_TEMP):
    """Exports SVGs from a layer state container object to path"""
    states = getObjChildren(layerStateContainer)
    stateLabels = getLayerObjChildrenLabels(layerStateContainer)
    for i in states:
        objs = getLayerStateActiveObj(i)
        i.Proxy.set_propertiesLayers()
        App.activeDocument().recompute(None,True,True)
        exportSVGFromProjection(objs,os.path.join(path, i.Label), getCamParam())
        d("exported: ", i.Label)


def exportSVGFromLayerState(layerState, path=USER_APP_PATH_TEMP):
    """Export SVG from layer state"""
    stateObjs = getLayerStateActiveObj(layerState)
    layerState.Proxy.set_propertiesLayers()
    App.activeDocument().recompute(None,True,True)
    d("recomputed", None)
    exportSVGFromProjection(stateObjs,os.path.join(path, layerState.Label), getCamParam())

def exportSvgFromSpreadsheetObj(sprObj, path=USER_APP_PATH_TEMP,
                                templatePath=TEMPLATE_FILE_PATH,
                                filenameSvg=None):

    """Exports SVGs of FreeCAD labels in Spreadsheet object
    """
    import csv
    app = App.ActiveDocument
    vecDic = {}
    tempCSVFile = open(os.path.join(USER_APP_PATH, 'temp.csv'), 'w+')
    tempCSVFilePath = tempCSVFile.name
    sprObj.exportFile(tempCSVFilePath)
    reader = csv.reader(tempCSVFile, delimiter='\t')
    vecDic = {row[3]: row[4] for row in reader}
    tempCSVFile.close()
    # delete title row
    del vecDic['FreeCAD Label']
    for label, vec in vecDic.items():
        labelt = trimLabelSuffix(label) + ".svg"
        dirCoor = extractDirVec(vec)
        viewDirVec = App.Base.Vector(dirCoor[0],
                                     dirCoor[1],
                                     dirCoor[2])
        xDirVec = App.Base.Vector(dirCoor[3],
                                  dirCoor[4],
                                  dirCoor[5])

        labelt = renameFileName(labelt)
        exportSvgImage(app.getObjectsByLabel(label)[0], path, templatePath,
                       labelt, viewDirVec, xDirVec)
    # remove temporary file
    try:
        os.remove(tempCSVFile.name)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))
    return vecDic


def extractDirVec(dirVecStr):
    """Creates list of coordinates from vector or vectors strings
    str -> list

    example:
    extractDirVec('Vector (-0.8331864476203918,
                  -0.4605315327644348, -0.30612263083457947)...')

    extractDirVec('[-0.8331864476203918, -0.4605315327644348,
                   -0.30612263083457947...]')
    """
    pattern = '[-]*[0-9][\.][0-9e-]*'
    vCoor = re.findall(pattern, dirVecStr)
    vCoor = [float(i) for i in vCoor]
    return vCoor


# # exportSvgImage(obj, path, filenameSvg=filename)
# def exportSvgImage(obj, path=USER_APP_PATH_TEMP, filenameSvg=''):

# this is the one used
def exportSvgImage(obj, path=USER_APP_PATH_TEMP,
                   templatePath=TEMPLATE_FILE_PATH,
                   filenameSvg='',
                   viewDirVec=None,
                   xDirVec=None,
                   isSimpleView=True,
                   lineThickness=0.5):
    # API change: precondition that obj is a single object and not a list
    # logDebug("new export SvgImage")
    if not filenameSvg:
        filenameSvg = obj.Label + '.svg'
    savePath = os.path.join(path, renameFileName(filenameSvg))
    # logDebug(savePath)

    from ..svg import svg
    svg.getSVGBOM(obj, savePath)

    return savePath

def exportSvgImageOld(obj, path=USER_APP_PATH_TEMP,
                   templatePath=TEMPLATE_FILE_PATH,
                   filenameSvg='',
                   viewDirVec=None,
                   xDirVec=None,
                   isSimpleView=True,
                   lineThickness=0.5):
    """Export Component, Body or Group to a pictographic SVG using TechDraw
    Workbench

    obj | list of obj, str, str, str -> str

    obj: FreeCAD object or list of FreeCAD objects
    path: path to save svg file
    templatePath: path to template
    filenameSvg: filename of image e.g. 'foo'

    Returns final save path

    Notes:
    - if refactored, need to edit template
    location
    - supports Group objects

    """
    from TechDrawGui import exportPageAsSvg as tdExport
    app = App.ActiveDocument
    pageObj = createTDPageFromObj(obj, isSimpleView=isSimpleView,
                                  lineThickness=0.5,
                                  viewDirVec=viewDirVec, xDirVec=xDirVec)
    if not filenameSvg:
        if type(obj) is list:
            filenameSvg = 'freecad-camera-view.svg'
        else:
            filenameSvg = obj.Label + '.svg'
    else:
        filenameSvg = filenameSvg
    savePath = os.path.join(path, renameFileName(filenameSvg))
    logDebug(savePath)

    # TODO: fix this workaround that handles 'TypeError' Page not Found
    # pageObj.ViewObject.doubleClicked()
    # pageObj.Visibility = False
    # 15 Sep 2023
    # This fix does not seem to work on main FreeCAD.  Perhaps this:
    c = App.Console
    c.PrintMessage(f'doubleClicked: {pageObj.ViewObject.doubleClicked()}\n')
    pageObj.Visibility = False

    tdExport(pageObj, savePath)
    #app.removeObject(pageObj.Name)
    return savePath


#this is the one used
def exportEachSvgOfGroup(layerObj, path=USER_APP_PATH_TEMP, unique=True):
    """exports svg of all children of a group object (e.g. layer)

    obj, str, bool -> list

    layerObjc: group object
    path: path where to save, example: '/home/foo/dir
    unique: if False, then export all children objects of group

    Returns: list of labels

    Supported objects:
    - layer object
    - group object
    - part object

    Note:
    - does not support layer group
    """
    app = App.ActiveDocument
    if unique:
        # labels = keepUniqueLabels(getObjChildrenProperties(layerObj,'Label'))
        # labels = keepUniqueLabels(getObjChildPropRec(layerObj, 'Label',
        #                                              'Part::Feature'))
        labels = getExampleLabels(getObjChildPropRec(layerObj, 'Label',
                                                     'Part::Feature'))
        for label in labels:
            obj = app.getObjectsByLabel(label)[0]
            filename = canonicalLabel(label) + '.svg'
            filename = renameFileName(filename)
            exportSvgImage(obj, path, filenameSvg=filename)
        return labels.sort()
    else:
        # labels = getObjChildrenProperties(layerObj, 'Label')
        labels = getObjChildPropRec(layerObj, 'Label', 'Part::Feature')
        for child in getObjChildren(layerObj):
            exportSvgImage(child, path)
        return labels


# TODO: fix camera position
def exportLayerState2DProjectionSVG(layerState, path=USER_APP_PATH_TEMP,
                                    filename='2dproj.svg'):
    from Draft import make_shape2dview
    import importSVG
    app = App.ActiveDocument
    objs = getLayerStateActiveObj(layerState)
    svgGroup = app.addObject('App::DocumentObjectGroup',
                             'svgGroup')
    for obj in objs:
        svgGroup.addObject(app.copyObject(obj))
    viewDirVec = getDirVec()[0].multiply(-1)
    shape2dview = make_shape2dview(svgGroup, viewDirVec)
    app.recompute()
    if not filename:
        filename = layerState.Label + '.svg'
    savePath = os.path.join(path, renameFileName(filename))
    importSVG.export([shape2dview], savePath)


def createTDPageFromObj(obj,
                        templatePath=TEMPLATE_FILE_PATH,
                        viewDirVec=None,
                        xDirVec=None,
                        label='lstd:',
                        isSimpleView=True,
                        lineThickness=0.5):
    """ Creates TD Page from obj or list of obj
    obj | list -> obj

    Notes:
    - if refactored, need to edit template
    location
    - supports Group objects

    """
    # from TechDrawGui import export as tdExport
    doc = Gui.ActiveDocument
    app = App.ActiveDocument

    pageObj = app.addObject('TechDraw::DrawPage', 'Page')
    templateObj = app.addObject('TechDraw::DrawSVGTemplate', 'Template')
    templateObj.Template = templatePath
    #pageObj.Template = templateObj
    pageObj.Template = app.Template
    pageObj.ViewObject.doubleClicked()
    get3dview(getMainWindow())
    drawViewObj = app.addObject('TechDraw::DrawViewPart', 'View')
    logDebug(f'1 x: {drawViewObj.X}')
    app.recompute()
    pageObj.addView(drawViewObj)
    logDebug(f'2 x: {drawViewObj.X}')
    if type(obj) is list:
        drawViewObj.Source = obj
    else:
        drawViewObj.Source = [obj]
    logDebug(f'3 x: {drawViewObj.X}')
    if (viewDirVec and xDirVec) is None:
        viewDirVec = doc.ActiveView.getViewDirection().multiply(-1)
        xDirVec = getXDirection()
    logDebug(f'4 x: {drawViewObj.X}')
    drawViewObj.Direction = viewDirVec
    logDebug(f'5 x: {drawViewObj.X}')
    drawViewObj.XDirection = xDirVec
    logDebug(f'6 x: {drawViewObj.X}')
    if isSimpleView:
        drawViewObj.SmoothVisible = False
        drawViewObj.ViewObject.LineWidth = lineThickness
    app.recompute()
    logDebug(f'7 x: {drawViewObj.X}')
    pageObj.Label = str(label)
    return pageObj


def exportTDobjectSVG(pageObj, path=USER_APP_PATH_TEMP, filename='test.svg'):
    """export svg image of pageObj
    obj -> str
    """
    from TechDrawGui import export as tdExport

    # TODO: fix this workaround that handles 'TypeError' Page not Found
    pageObj.ViewObject.doubleClicked()
    pageObj.Visibility = False

    savePath = os.path.join(path, renameFileName(filename))
    tdExport([pageObj], savePath)
    logMessage("Exporting to: " + savePath, 'w')
    return savePath


def exportActiveViewToSvg(mode=0, path=USER_APP_PATH_TEMP, filename='test.svg'):
    """
    export svg image of active view
    """
    from TechDrawGui import getActiveViewSvg
    filepath = os.path.join(path, renameFileName(filename))
    f = open(filepath, 'w')
    f.write(getActiveViewSvg(App.ActiveDocument, mode).decode('utf-8'))
    f.close()

# f.write(svg.getSVG(xdir, bottomLeft, [o for o, s in objects], cp,length, height))
def exportSVGFromProjection(objArray=[],
                            filePath=os.path.join(USER_APP_PATH_TEMP,'tmp.svg'),
                            cam=''):
    """uses projection to export SVG
    list of obj, str -> int
    example:
    exportSVGFromProjection(camera.getActiveViewObjects(),"/home/foo/a.svg")
    """
    if not cam:
        cameraBottomLeft, cameracp, dir, place, xdir, length, height = getCamParam()
    else:
        cameraBottomLeft, cameracp, dir, place, xdir, length, height = cam


    resolvePath(os.path.dirname(filePath))
    # d("length", length)
    # d("height", height)
    # d("obj", objArray)
    # d("cp", cameracp)
    # d("xdir", getXDirection())
    # d("bottomleft", cameraBottomLeft)
    # d("placement", place)
    camera.alignToPointAndAxis(dir)
    f = open(filePath, "w")
    f.write("<svg\n")
    f.write(f"       width=\"{length}\"\n")
    f.write(f"       height=\"{height}\"\n")
    f.write("	xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"\n")
    f.write("	xmlns:freecad=\"http://www.freecadweb.org/wiki/index.php")
    f.write("?title=Svg_Namespace\">\n")
    f.write(getSVG(xdir, cameraBottomLeft, objArray, cameracp,
                   length, height, place))
    f.write("\n</svg>")
    f.close()

    d("file saved succesfully at: ", filePath)

    return 0

def getCamParam():
    dir = camera.getViewDir()
    xdir = getXDirection()
    place = camera.getPlacement()
    cameraBottomLeft = camera.getCameraCornerVectors()[2]
    cameracp = camera.getCameraCenterPoint()
    length, height = camera.getCameraDimensions()[0], camera.getCameraDimensions()[1]
    return cameraBottomLeft,cameracp,dir,place,xdir, length, height


# =====================================
# Layer BOM Functions
# =====================================


def createLayerGroupBom(layerGroupObj, path=USER_APP_PATH_TEMP):
    """Creates BOM from layers in layer group object

    obj, str -> None

    - exports to path each layer's objects' SVGs
    - creates CSV of all layer group's contents
    - resolves duplicate parts automatically
    """
    exportLayerGroupSvg(layerGroupObj, path)
    createLayerGroupCSV(layerGroupObj, path)
    return None


# this is the one used
def createLayerObjBom(layerObj, path=USER_APP_PATH_TEMP):
    """Creates BOM from layer object

    """
    layerObjLabel = renameFileName(layerObj.Label)
    savePath = os.path.join(path, layerObjLabel)
    resolvePath(savePath)
    exportEachSvgOfGroup(layerObj, savePath)
    createLayerObjCSV(layerObj, savePath)


def createLayerGroupCSV(groupObj, path=USER_APP_PATH_TEMP, filename='bom.csv'):
    """Creates CSV BOM

    str, str -> None

    """
    import csv
    resolvePath(path)
    app = App.ActiveDocument
    sheetObj = app.addObject('Spreadsheet::Sheet',
                             groupObj.Label + '_Bom')
    logMessage("Exporting bom.csv to: " + path)
    bomDic = createLayerGroupObjChildrenDic(groupObj)
    trimPattern = '(?= [0-9]{3}$).{4}$'
    # CSV processing
    bomCsvFile = open(os.path.join(path, filename), "w")
    writer = csv.writer(bomCsvFile, delimiter='\t')
    writer.writerow(["Step",
                     "Part",
                     "Number",
                     "FreeCAD Label",
                     "Direction Coordinates",
                     "Filename"])
    for step, subdic in bomDic.items():
        for part, value in subdic.items():
            filename = renameFileName(re.sub(trimPattern, r'', part)) + ".svg"
            writer.writerow([step,
                             re.sub(trimPattern, r'', part),
                             value,
                             part,
                             getDirVecCoor(),
                             filename
                             ])
    bomCsvFile.close()
    sheetObj.importFile(bomCsvFile.name)
    app.recompute()
    logMessage("finished creating csv")
    return None


def createLayerObjCSV(layerObj, path=USER_APP_PATH_TEMP, filename=None):
    """Creates layerobject BOM CSV"""

    import csv
    resolvePath(path)
    if filename is None:
        filename = str(layerObj.Label + '.csv')
        filename = renameFileName(filename)
    app = App.ActiveDocument
    sheetObj = app.addObject('Spreadsheet::Sheet',
                             layerObj.Label + '_bom')
    logMessage("Exporting bom.csv to: " + path + '/' + filename)
    bomDic = createGroupObjChildrenDic(layerObj)
    trimPattern = '(?= [0-9]{3}$).{4}$'
    # CSV processing
    bomCsvFile = open(os.path.join(path, filename), "w")
    writer = csv.writer(bomCsvFile, delimiter='\t')
    writer.writerow(["Step",
                     "Part",
                     "Number",
                     "FreeCAD Label",
                     "Direction Coordinates",
                     "Filename"])
    for part, value in bomDic.items():
        filename = renameFileName(re.sub(trimPattern, r'', part)) + ".svg"
        writer.writerow([layerObj.Label,
                         re.sub(trimPattern, r'', part),
                         value,
                         part,
                         getDirVecCoor(),
                         filename
                         ])
    bomCsvFile.close()
    sheetObj.importFile(bomCsvFile.name)
    app.recompute()
    # logMessage("finished creating csv")
    return None


# =====================================
# Layer state helper
# =====================================

def getLayerStateActiveObj(layerStateObj):
    """Returns list of objects active in layer state
    obj -> obj
    """
    app = App.ActiveDocument
    layerInfoObjGroup = layerStateObj.Group
    activeLayerNames = []
    for layerInfoObj in layerInfoObjGroup:
        if layerInfoObj.Visible:
            activeLayerNames.append(layerInfoObj.NameLayer)
    objs = []
    for layerName in activeLayerNames:
        layerChildren = app.getObject(layerName).Group
        for child in layerChildren:
            if hasattr(child, 'Line'):
                objs.append(child)
                if child.Line:
                    objs.append(child.Line)
            else:
                objs.append(child)
    # hacky remove objects linked to position objects and only keep position
    # objects
    filteredLabels = []
    patternPosLabel = "^Pos of.*"
    for obj in objs:
        if re.match(patternPosLabel, obj.Label):
            # filteredLabels.append(re.sub(patternPosLabel, '', obj.Label))
            filteredLabels.append(obj.ObjectToPosition.Label)
    for label in filteredLabels:
        for obj in objs:
            if obj.Label == label:
                objs.remove(obj)
    # svgfn = layerStateObj.Label + '.svg'
    return objs

# =====================================
# Layer Helpers
# =====================================


def createLayerGroupObjChildrenDic(layerGroupObj):
    """Returns dictionary of layer group children's contents

    obj -> Dictionary

    example:
    > createLayerGroupobjchildrendic(foo)
    {'Parts Step 1': {'B-screw M6-12': 12, ...},
    'Parts Step 2': {'B-screw M6-10': 16, ...}}

    """
    bomDic = {}
    for layer in layerGroupObj.Group:
        bomDic[layer.Label] = createGroupObjChildrenDic(layer)
    return bomDic


def exportLayerGroupSvg(layerGroupObj, path=USER_APP_PATH_TEMP):
    """Export svg of layer group to path

    obj, str -> None

    """
    layerObjects = getObjChildren(layerGroupObj)
    for layer in layerObjects:
        layerLabel = renameFileName(layer.Label)
        savePath = os.path.join(path, layerLabel)
        resolvePath(savePath)
        exportEachSvgOfGroup(layer, savePath)


def getLayerGroupChildrenNames(layerGroupObj):
    """Returns: list of Layers Names

    obj -> list

    """
    layerNames = []
    for layer in layerGroupObj.Group:
        layerNames.append(layer.Name)
    return layerNames


def getLayerObjChildrenLabels(layerObj):
    """Returns list of objects labels

    obj -> list

    """
    layerChildrenLabels = []
    for layerChild in layerObj.Group:
        layerChildrenLabels.append(layerChild.Label)
    return layerChildrenLabels

# The pattern to increment labels
PATTERN_LABEL_INC = r'\s?[0-9]{3}$'

def canonicalLabel(label):
    return re.sub(PATTERN_LABEL_INC, r'', label)


def countLabels(labels, patterns):
    mapping = {k: 0 for k in patterns}

    for label in labels:
        cl = canonicalLabel(label)
        if cl in patterns:
            mapping[cl] += 1
    return mapping


def getExampleLabels(labels):
    #patterns = getPatternsFromLabels(labels)
    mapping = {}

    for label in labels:
        cl = canonicalLabel(label)
        if cl not in mapping:
            mapping[cl] = label
    return list(mapping.values())


# def countLabels(labels, matchPattern):
#     """
# > countLabels(['a 001', 'a 002', 'a 003',
#     'a 004', 'b 001', 'b 002', 'b001 001'],
#                    ['a', 'b', 'b001'])
# {'a': 4, 'b': 2, 'b001': 1}

# countLabels(["Link050(Soil)"],["Link050(Soil)"])

#     """

#     count = 0
#     dicL = {}
#     dicKey = ""
#     print("labels", labels)
#     print("matchPattern", matchPattern)
#     for pattern in matchPattern:
#         pattern = escape_string(pattern)
#         for label in labels:
#             regex_pattern = '(' + pattern + '$' + '|' + pattern + ' ' + ')'
#             if re.findall(regex_pattern, label):
#                 count = count + 1
#                 if count == 1:
#                     dicKey = label
#         try:
#             dicL[dicKey] = count
#             count = 0
#         except:
#             raise ValueError('unable to retrieve labels')
#     return dicL

def unescape_string(string):
    return re.sub(r'\\([\(\)\[\]\{\}])', r'\1', string)

def escape_string(string):
    return re.sub(r'([\(\)\[\]\{\}])', r'\\\1', string)


def getPatternsFromLabels(labels):
    """Trim list of string labels

    list, bool -> list


    example:
    > getPatternsFromLabels(['a 001', 'a 002', 'b 032', 'b033',
      'c 011', 'c 022'])
    ['a', 'b', 'c']

    """
    patterns = set()
    for label in labels:
        patterns.add(canonicalLabel(label))
    return list(patterns)

def keepUniqueLabels(listLabels):
    uniqueLabels = []
    dicLabels = countLabels(listLabels, getPatternsFromLabels(listLabels))
    for key in dicLabels:
        uniqueLabels.append(key)
    return uniqueLabels


def trimLabelSuffix(label: str):
    """Trim suffix numbers in repeated label objects

    """
    trimPattern = '(?= [0-9]{3}$).{4}$'
    return re.sub(trimPattern, r'', label)


# =====================================
# Camera helpers
# =====================================


def getXDirection2():
    """Returns XDirection of camera view using another method.

    None -> vect

    XDirection is used to create a coordinate system for the projection.

    Note:
    - depends on exposed getUpDirection function in FreeCAD
    (not available in core release)

    """
    doc = Gui.ActiveDocument
    viewDirVec = doc.ActiveView.getViewDirection()
    viewDirVec = viewDirVec * (-1)
    viewUpVec = doc.ActiveView.getUpDirection()
    viewXdirVec = viewUpVec.cross(viewDirVec)
    return viewXdirVec

def getXDirection():
    """Returns XDirection of camera view
    None -> vect
    """
    view = Gui.ActiveDocument.ActiveView
    dir = view.getViewDirection().negative()
    camera = view.getCameraNode()
    rot = camera.getField("orientation").getValue()
    coin_up = coin.SbVec3f(0, 1, 0)
    upvec = App.Vector(rot.multVec(coin_up).getValue())
    xdir = upvec.cross(dir)
    return xdir

def getDirVec():
    """Return direction vectors

    None -> List

    Returns:
    - list of vectors
      - view direction vector
      - x direction vector
    """
    doc = Gui.ActiveDocument
    # Techdraw uses negative direction vectors
    viewDirVec = doc.ActiveView.getViewDirection().multiply(-1)
    xDirVec = getXDirection()
    return [viewDirVec, xDirVec]


def getDirVecCoor():
    """Returns list of coordinates of dir vectors

    [viewDirVec, xDirVec]
    [x,y,z,x,y,z]
    """
    vl = getDirVec()
    return [vl[0].x, vl[0].y, vl[0].z,
            vl[1].x, vl[1].y, vl[1].z]


# =====================================
# Helper functions
# =====================================


def resolvePath(path):
    """Creates directories if path does not exist

    str -> str

    path: path to be resolved e.g. '/home/foo/foo'
    """
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def getFileParentPath(levels=0):
    """Get file's parent directory
    """
    pDir = os.path.split(os.path.dirname(__file__))[0]
    for i in range(0, levels):
        pDir = os.path.split(pDir)[0]
    return pDir

def moveSelection():
    """
    e.g. select two items in order: 1-from, 2-to
    moveSelection(FreeCADGui.Selection.getSelection())
    """
    objs = Gui.Selection.getSelection()
    origin = objs[0]
    originalPlacement = origin.Placement
    target = objs[1]
    origin.Placement = target.Placement
    return originalPlacement

# =====================================
# View Tools
# =====================================

def getMainWindow():
    """gets main window object from QtGui
    """
    topLevel = PySide.QtGui.QApplication.topLevelWidgets()
    for i in topLevel:
        if i.metaObject().className() == "Gui::MainWindow":
            return i
    raise Exception("No MainWindow found")


def get3dview(mainWindowObj):
    """gets 3d view object from QtGui
    """
    children = mainWindowObj.findChildren(PySide.QtGui.QMainWindow)
    for child in children:
        if child.metaObject().className() == "Gui::View3DInventor":
            return child
    return None


def is3dView():
    activeView = Gui.ActiveDocument.activeView()
    return type(activeView).__name__ == 'View3DInventorPy'
