## SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later


import os

import FreeCAD as App
import FreeCADGui as Gui
import PySide.QtGui
import draftobjects

from .bom_tools import (createLayerGroupBom,
                        createLayerGroupCSV,
                        createLayerObjCSV,
                        createLayerObjBom,
                        exportSvgFromSpreadsheetObj,
                        getDirVecCoor,
                        getLayerStateActiveObj,
                        createTDPageFromObj,
                        is3dView,
                        moveSelection)
from .bom_tools import USER_APP_PATH
from .. import ICONPATH
from ..util.debug import logDebug
from ..util.debug import logMessage


class CreateBom(object):
    """Creates a BOM from layer groups
    """

    command_name = 'OSHAutoDoc_CreateBom'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select Layer Group objects to create a BOM directory\n"
                       "- SVG images and a CSV file will be created")

        return {'Pixmap': os.path.join(ICONPATH, 'CreateBom.svg'),
                'MenuText': 'Create layer group BOM in directory of choice',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        qFileDialog = PySide.QtGui.QFileDialog
        if not Gui.Selection.getCompleteSelection():
            logMessage("Please select a Layer Group object")
        for obj in Gui.Selection.getCompleteSelection():
            if isinstance(obj.Proxy, draftobjects.layer.LayerContainer):
                bomSavePath = qFileDialog.getExistingDirectory()
                if bomSavePath != "":
                    createLayerGroupBom(obj, bomSavePath)
                else:
                    logMessage("No BOM files created")
            else:
                logMessage("Please select a layer group object", 'w')


        # dialog = Gui.PySideUic.loadUi(
        #     os.path.join(ICONPATH, 'CreateBom.ui'))

        # result = dialog.exec()
        # if result == dialog.Accepted:
        #     bomPath = dialog.bomPath.text()
        #     for obj in Gui.Selection.getCompleteSelection():
        #         if isinstance(obj.Proxy, draftobjects.layer.LayerContainer):
        #             if bomPath != "":
        #                 createLayerGroupCSV(obj, bomPath)
        #             else:
        #                 createLayerGroupCSV(obj)


# This is the one I'm using
class CreateBomFromLayers(object):
    """Creates a BOM from selected layer objects
    """

    command_name = 'OSHAutoDoc_CreateBomFromLayers'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select Layer objects to create BOM\n"
                       "- SVG images and a CSV file will be created")

        return {'Pixmap': os.path.join(ICONPATH, 'CreateBomFromLayers.svg'),
                'MenuText': 'Create layer(s) BOM in directory of choice',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        qFileDialog = PySide.QtGui.QFileDialog
        if not Gui.Selection.getSelection():
            logMessage("Please select a Layer Group object")
            return
        bomSavePath = qFileDialog.getExistingDirectory()
        for obj in Gui.Selection.getSelection():
            if isinstance(obj.Proxy, draftobjects.layer.Layer):
                if bomSavePath != "":
                    createLayerObjBom(obj, bomSavePath)
                else:
                    logMessage("No Layer object BOM created")
            else:
                logMessage("Please select a layer group object", 'w')


class CreateBomCSV(object):
    """Creates only a CSV BOM from layer group without exporting SVGs
    """

    command_name = 'OSHAutoDoc_CreateBomCSV'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Save BOM CSV of Layer Group in directory of choice\n"
                       "- Saves a CSV file"
                       "- Creates a BOM FreeCAD Sheet object")

        return {'Pixmap': os.path.join(ICONPATH, 'CreateBomCSV.svg'),
                'MenuText': 'Export CSV BOM from layer group',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        qFileDialog = PySide.QtGui.QFileDialog
        if not Gui.Selection.getCompleteSelection():
            logMessage("Please select a Layer Group object")
        for obj in Gui.Selection.getCompleteSelection():
            if isinstance(obj.Proxy, draftobjects.layer.LayerContainer):
                try:
                    # PySide
                    filepath, fileExtension = \
                        qFileDialog.getSaveFileName(None,
                                                    "Save BOM CSV",
                                                    dir=USER_APP_PATH,
                                                    filter="*.csv")
                except Exception:
                    # PyQt4
                    filepath = qFileDialog.getSaveFileName(None,
                                                           "Save BOM CSV",
                                                           dir=USER_APP_PATH,
                                                           filter="*.csv")
                if filepath != "":
                    filename = os.path.basename(os.path.realpath(filepath))
                    path = os.path.dirname(os.path.realpath(filepath))
                    createLayerGroupCSV(obj, path, filename)
                else:
                    logMessage("No BOM CSV file exported")

            else:
                logMessage("Please select a layer group object", 'w')


class CreateBomCSVFromLayers(object):
    """Creates a BOM csv file from selected part layer objects
    """

    command_name = 'OSHAutoDoc_CreateBomCSVFromLayers'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select layers to create BOM CSVs in directory of choice\n"
                       "- Saves a CSV file"
                       "- Creates a BOM FreeCAD Sheet object")

        return {'Pixmap': os.path.join(ICONPATH, 'CreateBomCSVFromLayers.svg'),
                'MenuText': 'Export CSV BOM from layer(s) selection',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        qFileDialog = PySide.QtGui.QFileDialog
        if not Gui.Selection.getCompleteSelection():
            logMessage("Please select one or more layers")
        filepath = qFileDialog.getExistingDirectory()
        for obj in Gui.Selection.getCompleteSelection():
            if isinstance(obj.Proxy, draftobjects.layer.Layer):
                if filepath != "":
                    createLayerObjCSV(obj, filepath)
                else:
                    logMessage("No CSV files exported")

            else:
                logMessage("Please select one or more layer objects", 'w')


class ExportSVGFromBomCSV(object):
    """Creates a BOM csv file from selected part layer objects
    """

    command_name = 'OSHAutoDoc_ExportSVGFromBomCSV'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select BOM SpreadSheet to export SVG files\n"
                       "- Saves SVGs of parts in BOM Spreadsheet"
                       "- Takes into account direction vectors")

        return {'Pixmap': os.path.join(ICONPATH, 'ExportSVGFromBomCSV.svg'),
                'MenuText': 'Export SVG file from SpreadSheet object',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        qFileDialog = PySide.QtGui.QFileDialog
        if not Gui.Selection.getCompleteSelection():
            logMessage("Please select one or more layers")
        filepath = qFileDialog.getExistingDirectory()
        for obj in Gui.Selection.getCompleteSelection():
            if obj.TypeId == 'Spreadsheet::Sheet':
                if filepath != "":
                    exportSvgFromSpreadsheetObj(obj, filepath)
                else:
                    logMessage("No SVG files exported")

            else:
                logMessage("Please select one or more Spreadsheet objects", 'w')


class CopyCameraVectorsToClipboard(object):
    """Copies camera vectors to clipboard
    """
    command_name = 'OSHAutoDoc_CopyCameraVectorsToClipboard'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Click button to copy camera vectors to clipboard")

        return {'Pixmap': os.path.join(ICONPATH,
                                       'CopyCameraVectorsToClipboard.svg'),
                'MenuText': 'Copy camera position vectors to clipboard',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        activeView = Gui.ActiveDocument.activeView()
        Gui.ActiveDocument.ActiveView
        qCliboardObj = PySide.QtGui.QClipboard()
        if type(activeView).__name__ == 'View3DInventorPy':
            cameraVec = getDirVecCoor()
            qCliboardObj.setText(str(cameraVec))
        else:
            logMessage("Please change to camera view")


class CreateLayerStateTD(object):
    """Creates layer state technical drawing object
    """

    command_name = 'OSHAutoDoc_CreateLayerStateTD'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select layer state to create a TD from it")

        return {'Pixmap': os.path.join(ICONPATH, 'CreateLayerStateTD.svg'),
                'MenuText': 'Create TD from Layer State',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        if is3dView():
            if not Gui.Selection.getCompleteSelection():
                logMessage("Please select a Layer State")
            for obj in Gui.Selection.getCompleteSelection():
                if type(obj.Proxy).__name__ == 'LayerState':
                    tdLabel = 'td:' + obj.Label
                    layerStateObjs = getLayerStateActiveObj(obj)
                    createTDPageFromObj(layerStateObjs,
                                        label=tdLabel)
                else:
                    logMessage("Please select one or more layer state objects", 'w')
        else:
            logMessage("Please change to Camera view", 'w')

class MoveToSelection(object):
    """Sets new placement from one object to another object placement
    """

    command_name = 'OSHAutoDoc_MoveToSelection'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        toolTipText = ("Select two objects to move one object to the placement of the other")

        return {'Pixmap': os.path.join(ICONPATH, 'MoveToSelection.svg'),
                'MenuText': 'Move an object to the location of another object',
                'ToolTip': toolTipText}

    def Activated(self):
        """
        """
        if is3dView():
            if not Gui.Selection.getCompleteSelection():
                logMessage("Please select two objects")
            objs = Gui.Selection.getCompleteSelection()
            if len(objs) == 2:
                moveSelection()
            else:
                logMessage("Please select two objects, to place one object to the other object's location", 'w')
        else:
            logMessage("Please change to Camera view", 'w')



# Add the command to the system
Gui.addCommand(CreateBom.command_name, CreateBom())
Gui.addCommand(CreateBomCSV.command_name, CreateBomCSV())
Gui.addCommand(CreateBomCSVFromLayers.command_name, CreateBomCSVFromLayers())
Gui.addCommand(CreateBomFromLayers.command_name, CreateBomFromLayers())
Gui.addCommand(ExportSVGFromBomCSV.command_name, ExportSVGFromBomCSV())
Gui.addCommand(CopyCameraVectorsToClipboard.command_name,
               CopyCameraVectorsToClipboard())
Gui.addCommand(CreateLayerStateTD.command_name, CreateLayerStateTD())
Gui.addCommand(MoveToSelection.command_name, MoveToSelection())
