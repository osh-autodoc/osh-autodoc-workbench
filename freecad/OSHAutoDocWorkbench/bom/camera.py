# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
import math

import FreeCADGui as Gui
import FreeCAD as App

import pivy.coin as coin

import DraftVecUtils
import Draft

def getCameraCornerVectors():
    """get vector coordinates of uleft,uright,bleft,bright corners in viewport

    The function viewer.getPoint() takes two arguments, The origin (0,0) is in
    the upper left corner.

    The function
    getSoRenderManager().getViewportRegion().getViewportSizePixels().getValue()
    gets the size of the viewport in pixels.

    Viewport in FreeCAD is the rectangular area where the 3D scene is displayed.
    """
    view = Gui.ActiveDocument.ActiveView
    viewer = view.Viewer
    sizePixels = viewer.getSoRenderManager()\
                       .getViewportRegion()\
                       .getViewportSizePixels().getValue()
    upperLeft = viewer.getPoint(0, 0)
    upperRight = viewer.getPoint(sizePixels[0], 0)
    bottomLeft = viewer.getPoint(0, sizePixels[1])
    bottomRight = viewer.getPoint(sizePixels[0], sizePixels[1])
    return [upperLeft, upperRight, bottomLeft, bottomRight]

def getCameraCenterPoint():
    """get current camera view center point

    nil -> Vector object"""
    view = Gui.ActiveDocument.ActiveView
    dir = view.getViewDirection().negative()
    v = App.Vector(dir.x, dir.y, dir.z)
    camera = view.getCameraNode()
    cam1 = App.Vector(camera.position.getValue().getValue())
    cam2 = Gui.ActiveDocument.ActiveView.getViewDirection()
    vcam1 = DraftVecUtils.project(cam1, v)
    a = vcam1.getAngle(cam2)
    if a < 0.0001:
        return App.Vector()
    _d = vcam1.Length
    L = _d/math.cos(a)
    vcam2 = DraftVecUtils.scaleTo(cam2, L)
    cp = cam1.add(vcam2)
    return cp

def getCameraDimensions():
    """getCameraDimensions gets the dimensions of the viewport of the window in
    pixels.

    nil -> [float, float]

    """
    viewerCorners = getCameraCornerVectors()
    upperRight = viewerCorners[1]
    bottomLeft = viewerCorners[2]
    upperLeft = viewerCorners[0]
    length = upperLeft.distanceToPoint(upperRight)
    height = upperLeft.distanceToPoint(bottomLeft)
    return [length, height]

def getActiveViewObjects():
   """return array of obj from current view screen
   None -> [array of obj]
   """

   viewer = Gui.ActiveDocument.ActiveView.Viewer

   objects = viewer.getPickedList(pos=getWindowPosCoor(),
                                       pickElement=False, mapCoords=False)
   return [obj for obj, string in objects]


def getWindowPosCoor():
   """returns coordinates of the screen viewport in the gui
   None -> [(x,y), (x,y)]
   """
   viewer = Gui.ActiveDocument.ActiveView.Viewer
   vpr = viewer.getSoRenderManager().getViewportRegion()
   viewportSizePixels = vpr.getViewportSizePixels().getValue()
   position = [coorToViewportScreenCoor(0,0),
               coorToViewportScreenCoor(viewportSizePixels[0],
                                      viewportSizePixels[1])]
   return position

def coorToViewportScreenCoor(x,y):
   """converts float to viewportscreen coordinates
   float, float -> (tuple)
   """
   coor = (x,y)
   viewer = Gui.ActiveDocument.ActiveView.Viewer
   vpr = viewer.getSoRenderManager().getViewportRegion()
   viewportAspectRatio = vpr.getViewportAspectRatio()
   viewportOriginPixels = vpr.getViewportOriginPixels().getValue()
   viewportSizePixels = vpr.getViewportSizePixels().getValue()
   viewportSize = vpr.getViewportSize().getValue()
   locX = coor[0] - viewportOriginPixels[0]
   locY = coor[1] - viewportOriginPixels[1]
   pX = locX / viewportSizePixels[0]
   pY = locY / viewportSizePixels[1]
   dX,dY = viewportSize[0], viewportSize[1]

   if viewportAspectRatio > 1.0:
       pX = (pX - 0.5 * dX) * viewportAspectRatio + 0.5 * dX
   elif viewportAspectRatio < 1.0:
       pY = (pY - 0.5 * dY) / viewportAspectRatio + 0.5 * dY
   return (pX, pY)

def alignToPointAndAxis(dir=''):
    if not dir:
        dir = getViewDir()
    cp = getCameraCenterPoint()
    camera = Gui.ActiveDocument.ActiveView.getCameraNode()
    rot = camera.getField("orientation").getValue()
    coin_up = coin.SbVec3f(0, 1, 0)
    upvec = App.Vector(rot.multVec(coin_up).getValue())
    def tostr(v):
        """Make a string from a vector or tuple."""
        string = "FreeCAD.Vector("
        string += str(v[0]) + ", "
        string += str(v[1]) + ", "
        string += str(v[2]) + ")"
        return string

    _cmd = "FreeCAD.DraftWorkingPlane.alignToPointAndAxis"
    _cmd += "("
    _cmd += tostr(cp) + ", "
    _cmd += tostr((dir.x, dir.y, dir.z)) + ", "
    _cmd += "0.0, "
    _cmd += tostr(upvec)
    _cmd += ")"
    Gui.doCommandGui(_cmd)

def getViewDir():
    view = Gui.ActiveDocument.ActiveView
    dir = view.getViewDirection().negative()
    return dir


def getPlacement():
    cp = getCameraCenterPoint()
    App.DraftWorkingPlane.setup()
    p = App.Placement(App.DraftWorkingPlane.getPlacement())
    p.Base = cp
    return p
