# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from PySide.QtCore import QT_TRANSLATE_NOOP

import FreeCAD as App

from draftobjects.layer import Layer

from ..util.util import findObjectsByType


class LayerInfo:

    def __init__(self, obj, layer):
        """Initialize a layer info object."""
        self.Type = "LayerInfo"
        self.Object = obj
        self.set_propertiesLayerInfo(obj, layer)

        obj.Proxy = self

    def printVersionWarning(self):
        c = App.Console
        c.PrintWarning("This file is based on OSHAutoDocWorkbench 0.3 ")
        c.PrintWarning("or lower\n")
        c.PrintWarning("Adding property Visible to LayerInfo\n")
        c.PrintWarning("Please save the file now to adapt to version 0.4.\n")

    def onDocumentRestored(self, obj):
        """Execute code when the document is restored.

        Add properties that don't exist.
        """
        self.Object = obj
        if "Visibility" in obj.PropertiesList:
            if "Visible" not in obj.PropertiesList:
                self.setVisibleProperty(obj)
                obj.Visible = obj.Visibility
                self.printVersionWarning()

            # It is not possible to remove 'Visibility'.  Apparently a Python
            # non-view object always has a Visbility property.  This is most
            # likely also why we observed the strange behavior (sometimes all
            # visibilities reset).
            #
            # obj.removeProperty('Visibility')
            # App.Console.PrintMessage(
            #     "Deleting property Visibility from LayerInfo\n")

    def set_propertiesLayerInfo(self, layerInfo, layer):
        """Set the properties of 'layerinfo' based on 'layer'
        """
        def ensure_prop(name, comment, typeProp):
            if name not in layerInfo.PropertiesList:
                _tip = QT_TRANSLATE_NOOP("App::Property", comment)
                layerInfo.addProperty(typeProp, name,
                                      "LayerInfo", _tip)

        ensure_prop("NameLayer", "The name of the layer",
                    "App::PropertyString")
        layerInfo.NameLayer = layer.Name

        self.set_properties(layerInfo, layer.ViewObject)

    def set_propertiesLayer(self):
        layerInfoObj = self.Object
        doc = App.ActiveDocument
        if doc is None:
            return
        layerObj = doc.getObject(layerInfoObj.NameLayer)
        layerVobj = layerObj.ViewObject

        def checkProperty(prop):
            if prop not in layerVobj.PropertiesList:
                App.Console.PrintError(f"Layer {layerVobj.Label} does not have"
                                       f"property {prop}")
                return False
            return True

        # override options
        if checkProperty("OverrideLineColorChildren") and \
           layerInfoObj.OverrideLineColorChildren != \
           layerVobj.OverrideLineColorChildren:
            layerVobj.OverrideLineColorChildren = \
                layerInfoObj.OverrideLineColorChildren

        if checkProperty("OverrideShapeColorChildren") and \
           layerInfoObj.OverrideShapeColorChildren != \
           layerVobj.OverrideShapeColorChildren:
            layerVobj.OverrideShapeColorChildren = \
                layerInfoObj.OverrideShapeColorChildren

        if checkProperty("UsePrintColor") and \
           layerInfoObj.UsePrintColor != layerVobj.UsePrintColor:
            layerVobj.UsePrintColor = layerInfoObj.UsePrintColor

        # visual properties
        if checkProperty("LineColor") and \
           layerInfoObj.LineColor != layerVobj.LineColor:
            layerVobj.LineColor = layerInfoObj.LineColor

        if checkProperty("ShapeColor") and \
           layerInfoObj.ShapeColor != layerVobj.ShapeColor:
            layerVobj.ShapeColor = layerInfoObj.ShapeColor

        if checkProperty("LineWidth") and \
           layerInfoObj.LineWidth != layerVobj.LineWidth:
            layerVobj.LineWidth = layerInfoObj.LineWidth

        if checkProperty("DrawStyle") and \
           layerInfoObj.DrawStyle != layerVobj.DrawStyle:
            layerVobj.DrawStyle = layerInfoObj.DrawStyle
            # probably fails

        if checkProperty("Transparency") and \
           layerInfoObj.Transparency != layerVobj.Transparency:
            layerVobj.Transparency = layerInfoObj.Transparency

        if checkProperty("LinePrintColor") and \
           layerInfoObj.LinePrintColor != layerVobj.LinePrintColor:
            layerVobj.LinePrintColor = layerInfoObj.LinePrintColor

        if checkProperty("LineColor") and \
           layerInfoObj.LineColor != layerVobj.LineColor:
            layerVobj.LineColor = layerInfoObj.LineColor

        if checkProperty("LineColor") and \
           layerInfoObj.LineColor != layerVobj.LineColor:
            layerVobj.LineColor = layerInfoObj.LineColor

        # other properties
        if checkProperty("Visibility") and \
           layerInfoObj.Visible != layerVobj.Visibility:
            layerVobj.Visibility = layerInfoObj.Visible

    def set_properties(self, setObj, readObj):
        """Set the properties only if they don't already exist."""

        self.set_override_options(setObj, readObj)
        self.set_visual_properties(setObj, readObj)
        self.set_other_properties(setObj, readObj)

    def ensure_property(self, setObj, readObj, prop, tip, typeProp):
        """Ensure an option exists in setObj based on readObj"""
        readObjProps = readObj.PropertiesList
        setObjProps = setObj.PropertiesList

        if prop in readObjProps:
            if prop not in setObjProps:
                _tip = QT_TRANSLATE_NOOP("App::Property", tip)
                setObj.addProperty(typeProp, prop, "LayerInfo", _tip)
            return True
        else:
            return False

    def set_override_options(self, setObj, readObj):
        """Set property options only if they don't already exist."""

        if self.ensure_property(setObj, readObj, "OverrideLineColorChildren",
                                "If it is true, the objects contained "
                                "within this layer info will adopt "
                                "the line color of the layer info",
                                "App::PropertyBool"):
            setObj.OverrideLineColorChildren = \
                readObj.OverrideLineColorChildren

        if self.ensure_property(setObj, readObj, "OverrideShapeColorChildren",
                                "If it is true, the objects contained "
                                "within this layer info will adopt "
                                "the shape color of the layer info",
                                "App::PropertyBool"):
            setObj.OverrideShapeColorChildren = \
                readObj.OverrideShapeColorChildren

        if self.ensure_property(setObj, readObj, "UsePrintColor",
                                "If it is true, the print color "
                                "will be used when objects in this "
                                "layer info are placed on a TechDraw page",
                                "App::PropertyBool"):
            setObj.UsePrintColor = readObj.UsePrintColor

    def set_visual_properties(self, setObj, readObj):
        """Set visual layerProps only if they don't already exist."""
        # view_group = App.ParamGet("User parameter:BaseApp/Preferences/View")

        if self.ensure_property(setObj, readObj, "LineColor",
                                "The line color of the objects "
                                "contained within this layer info",
                                "App::PropertyColor"):
            setObj.LineColor = readObj.LineColor

        if self.ensure_property(setObj, readObj, "ShapeColor",
                                "The shape color of the objects "
                                "contained within this layer info",
                                "App::PropertyColor"):
            setObj.ShapeColor = readObj.ShapeColor

        if self.ensure_property(setObj, readObj, "LineWidth",
                                "The line width of the objects contained "
                                "within this layer info",
                                "App::PropertyFloat"):
            setObj.LineWidth = readObj.LineWidth

        if self.ensure_property(setObj, readObj, "DrawStyle",
                                "The draw style of the objects contained "
                                "within this layer info",
                                "App::PropertyEnumeration"):
            setObj.DrawStyle = ["Solid", "Dashed", "Dotted", "Dashdot"]
            setObj.DrawStyle = readObj.DrawStyle

        if self.ensure_property(setObj, readObj, "Transparency",
                                "The transparency of the objects "
                                "contained within this layer info",
                                "App::PropertyPercent"):
            setObj.Transparency = readObj.Transparency

        if self.ensure_property(setObj, readObj, "LinePrintColor",
                                "The line color of the objects "
                                "contained within this layer info, "
                                "when used on a TechDraw page",
                                "App::PropertyColor"):
            setObj.LinePrintColor = readObj.LinePrintColor

    def setVisibleProperty(self, obj):
        _tip = QT_TRANSLATE_NOOP("App::Property",
                                 "Show the object in the 3D view")
        obj.addProperty("App::PropertyBool", "Visible", "LayerInfo",
                        _tip)

    def set_other_properties(self, setObj, readObj):
        """Set property options only if they don't already exist."""

        readObjProps = readObj.PropertiesList
        setObjProps = setObj.PropertiesList

        if "Visibility" in readObjProps:
            if "Visible" not in setObjProps:
                self.setVisibleProperty(setObj)
            setObj.Visible = readObj.Visibility

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return self.Type

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        if state:
            self.Type = state

    def execute(self, obj):
        """Execute when the object is created or recomputed. Do nothing."""
        pass

    def addObject(self, obj, child):
        """Add an object to this object if not in the Group property."""
        group = obj.Group
        if child not in group:
            group.append(child)
        obj.Group = group


class LayerState:
    """The LayerState object.

    It stores a list of LayerInfo objects that represents the information that
    is contained in a layer.
    """

    def __init__(self, obj):
        self.Type = "LayerState"
        self.Object = obj
        self.set_properties(obj)

        obj.Proxy = self

    def onDocumentRestored(self, obj):
        """Execute code when the document is restored.

        Add properties that don't exist.
        """
        self.Object = obj
        self.set_properties_view(obj)

    def addLayerInfo(self, obj, layer, useVisibilityLayers=True):
        """Add a layer info object.

        If useVisibilityLayers is true, then use the visibility of the layers,
        otherwise set the visibility of this layer info object to false.
        """
        doc = App.ActiveDocument
        new_obj = doc.addObject("App::FeaturePython", "LayerInfo")
        LayerInfo(new_obj, layer)
        group = obj.Group
        group.append(new_obj)
        obj.Group = group

        if not useVisibilityLayers:
            new_obj.Visible = False

        # hide the item in the tree
        new_obj.ViewObject.ShowInTree = False

    def removeLayerInfo(self, obj, objLayerInfo):
        doc = App.ActiveDocument

        group = obj.Group
        group.remove(objLayerInfo)
        obj.Group = group

        doc.removeObject(objLayerInfo.Name)

    def updateLayerInfos(self):

        obj = self.Object
        doc = App.ActiveDocument

        # add layer infos for layers that don't have a layer info
        for objLayer in findObjectsByType(Layer):
            if objLayer.Name not in map(lambda objLi: objLi.NameLayer,
                                        obj.Group):
                # Add a layer to this layer state but don't make it visible in
                # this layer state
                self.addLayerInfo(obj, objLayer, False)

        # remove layer infos for which no layer exists
        for objLi in obj.Group:
            if not doc.getObject(objLi.NameLayer):
                self.removeLayerInfo(obj, objLi)

    def set_properties(self, obj):
        """Set properties only if they don't exist."""
        if "Group" not in obj.PropertiesList:
            _tip = QT_TRANSLATE_NOOP("App::Property",
                                     "The objects that are part of "
                                     "this step group")
            obj.addProperty("App::PropertyLinkList",
                            "Group",
                            "LayerState",
                            _tip)

        for layer in findObjectsByType(Layer):
            self.addLayerInfo(obj, layer)

        if "Camera" not in obj.PropertiesList:
            _tip = QT_TRANSLATE_NOOP("App::Property",
                                     "The camera position")
            obj.addProperty("App::PropertyString",
                            "Camera",
                            "LayerState",
                            _tip)
            obj.Camera = ""

        self.set_properties_view(obj)

    def set_properties_view(self, obj):
        if "WidthView" not in obj.PropertiesList:
            _tip = QT_TRANSLATE_NOOP("App::Property",
                                     "The width of the camera view")
            obj.addProperty("App::PropertyInteger",
                            "WidthView",
                            "LayerState",
                            _tip)
            obj.WidthView = 0

        if "HeightView" not in obj.PropertiesList:
            _tip = QT_TRANSLATE_NOOP("App::Property",
                                     "The height of the camera view")
            obj.addProperty("App::PropertyInteger",
                            "HeightView",
                            "LayerState",
                            _tip)
            obj.HeightView = 0

    def set_propertiesLayers(self):
        """Set the properties of the layers according to all the layerInfos in this
        layer state

        Notice that the viewprovider has a similar method.
        """

        layerStateObj = self.Object
        for layerInfoObj in layerStateObj.Group:
            layerInfoObj.Proxy.set_propertiesLayer()

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return self.Type

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        if state:
            self.Type = state

    def execute(self, obj):
        """Execute when the object is created or recomputed. Do nothing."""
        self.updateLayerInfos()

    def addObject(self, obj, child):
        """Add an object to this object if not in the Group property."""
        group = obj.Group
        if child not in group:
            group.append(child)
        obj.Group = group

    def getLayerInfoByLayerName(self, layerName):
        for li in self.Object.Group:
            if li.NameLayer == layerName:
                return li
        return None


class LayerStateContainer:
    """The container object for layer states.

    This class is normally used to extend
    a base `App::DocumentObjectGroupPython` object.
    It is inspired by LayerContainer.
    """

    def __init__(self, obj):
        self.Type = "LayerStateContainer"
        obj.Proxy = self

    def execute(self, obj):
        """Execute when the object is created or recomputed.

        Update the value of `Group` by sorting the contained layer states
        by `Label`.
        """
        group = obj.Group
        group.sort(key=lambda layerState: layerState.Label)
        obj.Group = group

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        if hasattr(self, "Type"):
            return self.Type

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        if state:
            self.Type = state
