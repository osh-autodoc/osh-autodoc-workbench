# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

import FreeCADGui as Gui

from .. import ICONPATH

import draftguitools.gui_base as gui_base

from . import make_layer_state as mas


class LayerState(gui_base.GuiCommandSimplest):
    """GuiCommand to create an LayerState object in the document."""

    command_name = 'OSHAutoDoc_LayerState'

    def __init__(self):
        super(LayerState, self).__init__(name="LayerState")

    def GetResources(self):
        """Set icon, menu and tooltip."""
        return {'Pixmap': os.path.join(ICONPATH, 'state-icon.svg'),
                'MenuText': "Layer State",
                'ToolTip': "Adds an layer state to the document.\n" +
                "Added objects can share the same visual " +
                "properties such as line color, line width, and shape color."}

    def Activated(self):
        """Execute when the command is called.

        It calls the `finish(False)` method of the active Draft command.
        """
        super(LayerState, self).Activated()

        self.doc.openTransaction("Create LayerState")
        Gui.addModule("freecad.OSHAutoDocWorkbench")
        Gui.doCommand(mas.command_make_layer_state)
        Gui.doCommand('FreeCAD.ActiveDocument.recompute()')
        self.doc.commitTransaction()


Gui.addCommand(LayerState.command_name, LayerState())
