# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD as App
from .layer_state import (LayerState, LayerStateContainer)

if App.GuiUp:
    from .view_layer_state \
        import (ViewProviderLayerState,
                ViewProviderLayerStateContainer)


command_make_layer_state = '_layerState_ = ' + \
    'freecad.OSHAutoDocWorkbench.layer_state.make_layer_state.' + \
    'make_layer_state()'


def get_layer_state_container():
    """Return a group object to put layer states in.

    Returns
    -------
    App::DocumentObjectGroupPython
        The existing group object named `'LayerStateContainer'`
        of type `LayerStateContainer`.
        If it doesn't exist it will create it with this default Name.
    """
    doc = App.ActiveDocument
    if not doc:
        App.Console.PrintError("No active document. Aborting.")
        return None

    for obj in doc.Objects:
        if obj.Name == "LayerStateContainer":
            return obj

    new_obj = doc.addObject("App::DocumentObjectGroupPython",
                            "LayerStateContainer")
    new_obj.Label = "Layer States"

    LayerStateContainer(new_obj)

    if App.GuiUp:
        ViewProviderLayerStateContainer(new_obj.ViewObject)

    return new_obj


def make_layer_state(name=None):
    """Create an LayerState object in the active document.

    If an layer state container named `'LayerStateContainer'` does not
    exist, it is created with this name.

    Parameters
    ----------
    name: str, optional
        It is used to set the layer state's `Label` (user editable).
        It defaults to `None`, in which case the `Label` is set to
        `'LayerState'` or to its translation in the current language.


    Return
    ------
    App::FeaturePython
        A scripted object of type `'LayerState'`.
        This object does not have a `Shape` attribute.
        Modifying the view properties of this object will affect the objects
        inside of it.

    None
        If there is a problem it will return `None`.

    """

    doc = App.ActiveDocument

    if not doc:
        App.Console.PrintError("No active document. Aborting.")
        return None

    if name:
        if not isinstance(name, str):
            App.Console.PrintError(f"Wrong input: {name} must be a string")
            return None
    else:
        name = "LayerState"

    new_obj = doc.addObject("App::FeaturePython", "LayerState")
    LayerState(new_obj)

    new_obj.Label = name

    if App.GuiUp:
        ViewProviderLayerState(new_obj.ViewObject)

    container = get_layer_state_container()
    container.addObject(new_obj)

    return new_obj
