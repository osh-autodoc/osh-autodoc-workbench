# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

import pivy.coin as coin
import PySide.QtGui as QtGui

import FreeCAD as App
import FreeCADGui as Gui

from .. import ICONPATH
from .layer_state import LayerState
# from ..bom.bom_tools import getLayerStateActiveObj

# from ..util.debug import d
from ..util.debug import logDebug
from ..util.util import renameFileName
from ..svg.gui_export_svg import exportSVG


def getActiveSubWindow():
    mw = Gui.getMainWindow()
    cw = mw.centralWidget()
    return cw.activeSubWindow()


class ViewProviderLayerState:
    """The viewprovider for the LayerState object."""

    dirSaveSVG = ''

    def __init__(self, vobj):
        self.Object = vobj.Object

        vobj.Proxy = self

    def getIcon(self):
        """Return the path to the icon used by the viewprovider.
        """
        return os.path.join(ICONPATH, "state-icon.svg")

    def attach(self, vobj):
        """Set up the scene sub-graph of the viewprovider."""
        self.Object = vobj.Object
        sep = coin.SoGroup()
        vobj.addDisplayMode(sep, "Default")

    def claimChildren(self):
        """Return objects that will be placed under it in the tree view.

        These are the elements of the `Group` property of the Proxy object.
        """
        # if hasattr(self, "Object") and hasattr(self.Object, "Group"):
        #     return self.Object.Group
        return []

    def getDisplayModes(self, vobj):
        """Return the display modes that this viewprovider supports."""
        return ["Default"]

    def getDefaultDisplayMode(self):
        """Return the default display mode."""
        return "Default"

    def setDisplayMode(self, mode):
        """Return the saved display mode."""
        return mode

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return None

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        return None

    def updateData(self, obj, prop):
        """Execute when a property from the Proxy class is changed."""
        pass

    def change_view_properties(self, vobj, prop):
        """Iterate over the contents and change the properties."""
        pass

    def onChanged(self, vobj, prop):
        """Execute when a view property is changed."""
        pass

    def canDragObject(self, obj):
        """Return True to allow dragging one object from the layer state."""
        return True

    def canDragObjects(self):
        """Return True to allow dragging many objects from the
        layer state."""
        return True

    def dragObject(self, vobj, otherobj):
        """Remove the object that was dragged from the layer state."""
        if hasattr(vobj.Object, "Group") and otherobj in vobj.Object.Group:
            group = vobj.Object.Group
            group.remove(otherobj)
            vobj.Object.Group = group
            App.ActiveDocument.recompute()

    def canDropObject(self, obj):
        """Return true to allow dropping one object.

        If the object being dropped is itself a `'Layer State'`, return
        `False` to prevent dropping a layer state inside a layer state,
        at least for now.
        """
        if hasattr(obj, "Proxy") and isinstance(obj.Proxy, LayerState):
            return False
        return True

    def canDropObjects(self):
        """Return true to allow dropping many objects."""
        return True

    def dropObject(self, vobj, otherobj):
        """Add object that was dropped into the Layer State to the group.

        If the object being dropped is itself a `'LayerState'`,
        return immediately to prevent dropping an layer state inside an
        layer state,
        at least for now.
        """
        if hasattr(otherobj, "Proxy") and isinstance(otherobj.Proxy,
                                                     LayerState):
            return

        obj = vobj.Object

        if hasattr(obj, "Group") and otherobj not in obj.Group:
            group = obj.Group
            group.append(otherobj)
            obj.Group = group

            # Remove from all other layer states (not automatic)
            for parent in otherobj.InList:
                if (hasattr(parent, "Proxy")
                        and isinstance(parent.Proxy, LayerState)
                        and otherobj in parent.Group and parent != obj):
                    p_group = parent.Group
                    p_group.remove(otherobj)
                    parent.Group = p_group

            App.ActiveDocument.recompute()

    def setupContextMenu(self, vobj, menu):
        """Set up actions to perform in the context menu."""
        activateAction = QtGui.QAction(QtGui.QIcon(":/icons/button_right.svg"),
                                       "Activate this layer", menu)
        activateAction.triggered.connect(self.activate)
        menu.addAction(activateAction)

        activateLegacyAction = QtGui.QAction(
            QtGui.QIcon(":/icons/button_right.svg"),
            "Activate this layer (No window resizing)", menu)
        activateLegacyAction.triggered.connect(self.activateLegacy)
        menu.addAction(activateLegacyAction)

        saveCameraAction = QtGui.QAction(
            QtGui.QIcon(":/icons/view-perspective.svg"),
            "Save the camera position", menu)
        saveCameraAction.triggered.connect(self.saveCamera)
        menu.addAction(saveCameraAction)

        saveSvgAction = QtGui.QAction(
            QtGui.QIcon(":icons/view-perspective.svg"),
            "Save SVG image of camera view", menu)
        saveSvgAction.triggered.connect(self.saveSvg)
        menu.addAction(saveSvgAction)

    def activate(self):
        """Activate the selected layer state, it becomes the Autogroup."""
        layerStateObj = self.Object
        layerState = layerStateObj.Proxy
        layerState.set_propertiesLayers()
        self.set_propertiesLayers()

    def activateLegacy(self):
        """Activates layer state and restores camera view only"""
        layerStateObj = self.Object
        layerState = layerStateObj.Proxy
        layerState.set_propertiesLayers()
        self.set_propertiesLayers(legacy=True)

    def set_propertiesLayers(self, legacy=False):
        """Set the properties of the layers according to all the layerInfos in
        this layer state"""
        if legacy:
            self.restoreCameraOnly()
        else:
            self.restoreCamera()

    def saveCamera(self):
        doc = Gui.ActiveDocument
        if doc is not None:
            cameraJson = doc.ActiveView.getCamera()
            layerStateObj = self.Object
            layerStateObj.Camera = cameraJson
            asw = getActiveSubWindow()
            layerStateObj.WidthView = asw.width()
            layerStateObj.HeightView = asw.height()
            App.ActiveDocument.recompute()

    def restoreCamera(self):
        """"
        Restores camera and window size view
        """
        doc = Gui.ActiveDocument
        if doc is not None:
            layerStateObj = self.Object
            if layerStateObj.Camera != "":
                width = layerStateObj.WidthView
                height = layerStateObj.HeightView

                if width > 0 and height > 0:
                    asw = getActiveSubWindow()
                    asw.showNormal()
                    asw.resize(width, height)
                else:
                    App.Console.PrintWarning("This layer state has no valid "
                                             "width and height, please save "
                                             "the camera position again.\n")

                doc.ActiveView.setCamera(layerStateObj.Camera)

    def restoreCameraOnly(self):
        """
        Restores only camera view
        """
        doc = Gui.ActiveDocument
        if doc is not None:
            layerStateObj = self.Object
            if layerStateObj.Camera != "":
                doc.ActiveView.setCamera(layerStateObj.Camera)

    def getObjectsLayerState(self):
        layerStateObj = self.Object
        objects = []
        doc = App.ActiveDocument
        for liObj in layerStateObj.Group:
            layerObj = doc.getObject(liObj.NameLayer)
            if layerObj.Visibility:
                objects.extend(layerObj.Group)

        return objects

    def saveSvg(self, dir=None):
        # from ..bom.bom_tools import exportSVGFromProjection
        doc = Gui.ActiveDocument
        if doc is not None:
            self.activate()
            layerStateObj = self.Object
            svgfn = renameFileName(layerStateObj.Label + '.svg')

            if dir:
                # means that we are scripting it
                ViewProviderLayerState.dirSaveSVG = dir

            if ViewProviderLayerState.dirSaveSVG:
                filename = os.path.join(ViewProviderLayerState.dirSaveSVG,
                                        svgfn)
            else:
                filename = svgfn

            if dir:
                file = filename
            else:
                qFileDialog = QtGui.QFileDialog
                fp = qFileDialog.getSaveFileName(caption="Save SVG file",
                                                 dir=filename,
                                                 filter="SVG Images (*.svg)")
                file = fp[0]
            if file:
                ViewProviderLayerState.dirSaveSVG = os.path.split(file)[0]
                exportSVG(file, self.getObjectsLayerState())

    # def saveSvgLegacy(self):
    #     from ..bom.bom_tools import (exportSvgImage)
    #     # app = App.ActiveDocument
    #     doc = Gui.ActiveDocument
    #     qFileDialog = QtGui.QFileDialog
    #     filepath = qFileDialog.getExistingDirectory()

    #     if doc is not None:
    #         layerStateObj = self.Object
    #         objs = getLayerStateActiveObj(layerStateObj)
    #         svgfn = layerStateObj.Label + '.svg'
    #         exportSvgImage(objs, path=filepath,
    #                        filenameSvg=renameFileName(svgfn))

    def saveSvgHeadless(self):
        """
        exports svg of active state camera view to home folder using Draft wb
        """
        # TODO: figure out why projection vector exports upside down
        from Draft import make_shape2dview
        import importSVG
        from pathlib import Path
        doc = Gui.ActiveDocument
        if doc is not None:
            layerStateObj = self.Object
            logDebug(layerStateObj.Label)
            layerInfoObjGroup = layerStateObj.Group
            logDebug(layerInfoObjGroup[0].Label)
            activeLayerName = []
            for layerInfoObj in layerInfoObjGroup:
                if layerInfoObj.Visible:
                    activeLayerName.append(layerInfoObj.NameLayer)
            logDebug(activeLayerName)
            app = App.ActiveDocument
            # needed to project 2d view of all objects in state
            svgGroup = App.ActiveDocument.addObject('App::DocumentObjectGroup',
                                                    'svgGroup')
            for name in activeLayerName:
                layerChildren = app.getObject(name).Group
                for child in layerChildren:
                    svgGroup.addObject(app.copyObject(
                        app.getObject(child.Name)))
            viewDirVec = doc.ActiveView.getViewDirection().multiply(-1)
            shape2dview = make_shape2dview(svgGroup, viewDirVec)
            app.recompute()
            export_list = []
            filenameSvg = renameFileName(layerStateObj.Label + '.svg')
            savePath = str(Path.home()) + '/' + filenameSvg
            export_list.append(shape2dview)
            logDebug("Saving to" + savePath)
            importSVG.export(export_list, savePath)
            svgGroup.removeObjectsFromDocument()
            app.removeObject(svgGroup.Name)
            app.removeObject(shape2dview.Name)
            app.recompute()


class ViewProviderLayerStateContainer:
    """The viewprovider for the LayerStateContainer object."""
    def __init__(self, vobj):
        self.Object = vobj.Object
        vobj.Proxy = self

    def getIcon(self):
        """Return the path to the icon used by the viewprovider."""
        return os.path.join(ICONPATH, "state-container-icon.svg")

    def attach(self, vobj):
        """Set up the scene sub-graph of the viewprovider."""
        self.Object = vobj.Object

    def setupContextMenu(self, vobj, menu):
        """Set up actions to perform in the context menu."""
        pass

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return None

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        return None
