# SPDX-FileCopyrightText: 2019 Yorik van Havre <yorik@uncreated.net>
# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later
# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2019 Yorik van Havre <yorik@uncreated.net>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# Heavily based on the BimLayers.py from the BIM Workbench

"""Layer State manager for FreeCAD"""


# Since FreeCAD has many different objects that are related, we follow this
# convention:
# a DocumentObject has suffix 'Obj'
# a ViewObject has suffix 'Vobj'
# the associated implemented classes don't have a suffix
#
# Example:
# layerState is of type LayerState
# layerStateObj is of type App::FeaturePython
# layerStateVobj is of type 'View provider object'

import os

import FreeCAD as App
import FreeCADGui as Gui

from . import ICONPATH
from .util.util import findObjectsByType

from .layer_state.layer_state import LayerState
from .layer_state.make_layer_state import make_layer_state
# from .util.debug import d


COL_VISIBILITY = 0
COL_LAYER_LABEL = 1
COL_LINE_WIDTH = 2
COL_DRAW_STYLE = 3
COL_LINE_COLOR = 4
COL_SHAPE_COLOR = 5
COL_TRANSPARENCY = 6
COL_LINE_PRINT_COLOR = 7

PREF_WIDTH = "LayerStateManagerWidth"
PREF_HEIGHT = "LayerStateManagerHeight"


# dummy function for the QT translator
def QT_TRANSLATE_NOOP(ctx, txt):
    return txt


def translate(ctx, txt):
    return txt


def getColorIcon(color):
    "returns a QtGui.QIcon from a color 3-float tuple"

    # from PySide import QtCore,QtGui
    c = QtGui.QColor(int(color[0]*255), int(color[1]*255), int(color[2]*255))
    im = QtGui.QImage(48, 48, QtGui.QImage.Format_ARGB32)
    im.fill(c)
    px = QtGui.QPixmap.fromImage(im)
    return QtGui.QIcon(px)


def d(message):
    #App.Console.PrintMessage(message + '\n')
    pass


class LayerStateManager:
    "The Layer State Manager FreeCAD command"

    command_name = "OSHAutoDoc_Layer"

    def GetResources(self):

        return {'Pixmap': os.path.join(ICONPATH,
                                       "layer-state-manager-icon.svg"),
                'MenuText': QT_TRANSLATE_NOOP("LayerStateManager",
                                              "Manage layer states..."),
                'ToolTip': QT_TRANSLATE_NOOP("LayerStateManager",
                                             "Set/modify the "
                                             "different layer states")}

    def IsActive(self):
        return App.ActiveDocument is not None

    def Activated(self):

        import FreeCADGui as Gui
        # from PySide import QtCore,QtGui

        self.nameLayerStateDialog = Gui.PySideUic.\
            loadUi(os.path.join(ICONPATH, "NameLayerStateDialog.ui"))

        # create the dialog
        self.dialog = Gui.PySideUic.\
            loadUi(os.path.join(ICONPATH, "LayerStatesDialog.ui"))

        # set nice icons
        self.dialog.setWindowIcon(QtGui.QIcon(
            os.path.join(ICONPATH, "state-container-icon.svg")))

        self.dialog.buttonActivate.setIcon(QtGui.QIcon(":/icons/edit_OK.svg"))
        self.dialog.buttonNewLayerState.\
            setIcon(QtGui.QIcon(":/icons/document-new.svg"))
        self.dialog.buttonDeleteLayerState.\
            setIcon(QtGui.QIcon(":/icons/delete.svg"))

        self.dialog.buttonSelectAll.\
            setIcon(QtGui.QIcon(":/icons/edit-select-all.svg"))
        self.dialog.buttonToggle.\
            setIcon(QtGui.QIcon(":/icons/dagViewVisible.svg"))
        self.dialog.buttonBlack.\
            setIcon(QtGui.QIcon(os.path.join(ICONPATH, "black.svg")))
        self.dialog.buttonGrey.\
            setIcon(QtGui.QIcon(os.path.join(ICONPATH, "grey.svg")))

        # no icon for buttonClose

        # # restore window geometry from stored state
        pref = App.ParamGet("User parameter:BaseApp/Preferences/Mod/"
                            "OSHAutoDocWorkbench")
        w = pref.GetInt(PREF_WIDTH, 640)
        h = pref.GetInt(PREF_HEIGHT, 320)
        self.dialog.resize(w, h)

        # center the dialog over FreeCAD window
        mw = Gui.getMainWindow()
        self.dialog.move(mw.frameGeometry().topLeft() + mw.rect().center() -
                         self.dialog.rect().center())

        # connect signals/slots
        self.dialog.listWidget.currentItemChanged.connect(self.onStateSelected)
        self.dialog.buttonActivate.clicked.connect(self.onActivateLayerState)
        self.dialog.buttonNewLayerState.clicked.\
            connect(self.onAddNewLayerState)
        self.dialog.buttonDeleteLayerState.clicked.\
            connect(self.onDeleteLayerState)

        self.dialog.buttonSelectAll.clicked.connect(self.dialog.tree.selectAll)
        self.dialog.buttonToggle.clicked.connect(self.onToggle)
        self.dialog.buttonBlack.clicked.connect(self.onBlack)
        self.dialog.buttonGrey.clicked.connect(self.onGrey)

        self.dialog.buttonClose.clicked.connect(self.dialog.reject)
        self.dialog.rejected.connect(self.reject)

        # set the model up
        self.model = QtGui.QStandardItemModel()
        self.dialog.tree.setModel(self.model)
        self.dialog.tree.setUniformRowHeights(True)
        self.dialog.tree.setItemDelegate(LayerStateManagerDelegate())
        self.dialog.tree.setItemsExpandable(False)
        # removes spacing in first column
        self.dialog.tree.setRootIsDecorated(False)
        # allow to select many
        self.dialog.tree.setSelectionMode(QtGui.QTreeView.ExtendedSelection)

        # update the dialog
        self.updateDialog()

        # rock 'n roll!!!
        self.dialog.show()

    def getLayerStateByName(self, name):
        """Get the selected layer state by name.

        On any error, return None.
        """

        if name is None:
            return None

        doc = App.ActiveDocument
        if doc is not None:
            objs = doc.getObjectsByLabel(name)
            if len(objs) == 1:
                layerState = objs[0]
                return layerState
        return None

    def onStateSelected(self, current, previous):
        """Show the layer information in the manager based on layer state
        'current'.

        This means that the manager shows the layers as is stored in the layer
        state, but the document is not updated.  Only when the Activate button
        is clicked, is this information 'committed' to show the layers
        according to this layer state.
        """
        d(f'onStateSelected, current: {current}, previous {previous}')

        if previous:
            layerState = self.getLayerStateByName(previous.text())
            self.saveLayerState(layerState)
        if current:
            layerState = self.getLayerStateByName(current.text())
            self.updateLayers(layerState)

    def getSelectedLayerStateName(self):
        """Get the selected layer state name.

        On any error, return None
        """
        listWidget = self.dialog.listWidget
        selectedItems = listWidget.selectedItems()
        if len(selectedItems) == 1:
            return selectedItems[0].text()

        return None

    def getSelectedLayerState(self):
        return self.getLayerStateByName(self.getSelectedLayerStateName())

    def onActivateLayerState(self):
        """Activate the layer states.

        This means that the stored layer information is used to set up the
        layers after which the document is recomputed.
        """

        d('onActivateLayerState')
        doc = App.ActiveDocument

        layerStateObj = self.getSelectedLayerState()
        if layerStateObj is not None:
            self.saveLayerState(layerStateObj)

            layerState = layerStateObj.Proxy
            layerState.set_propertiesLayers()

            if App.GuiUp:
                layerStateVobj = layerStateObj.ViewObject
                layerStateVobj.Proxy.set_propertiesLayers()

        doc.recompute()

    def makeLayerState(self, name):

        """Make a layer state and add it to the document
        Returns the label name of the layer state.
        """
        doc = App.ActiveDocument

        if not doc:
            App.Console.PrintError("No active document. Aborting.")
            return None

        new_obj = make_layer_state(name)

        return new_obj.Label

    def onAddNewLayerState(self):
        """Ask for a layer state name and add the new layer state to the
        document"""
        nameDialog = self.nameLayerStateDialog
        mainDialog = self.dialog
        listWidget = mainDialog.listWidget

        d('onAddNewLayerState')

        result = nameDialog.exec()
        if result == nameDialog.Accepted:
            name = self.makeLayerState(nameDialog.nameLayerState.text())
            if name is not None:
                listWidget.addItem(name)
                previousItem = listWidget.currentItem()
                item = listWidget.findItems(name, QtCore.Qt.MatchExactly)[0]
                listWidget.setItemSelected(item, True)
                self.onStateSelected(item, previousItem)
            nameDialog.nameLayerState.clear()

    def onDeleteLayerState(self):
        """Delete the current layer state"""

        c = App.Console
        c.PrintMessage("TODO: onDeleteLayerState\n")
        c.PrintMessage("The layer state is: "
                       f"{self.getSelectedLayerState().Proxy}\n")

    def saveLayerState(self, layerState):
        """Save the layer state"""

        doc = App.ActiveDocument

        changed = False

        def checkChanged():
            nonlocal changed
            if not changed:
                App.ActiveDocument.openTransaction("LayerState change")
                changed = True

        def getCol(row, col):
            return self.model.item(row, col)

        # apply changes
        for row in range(self.model.rowCount()):

            # get the current layerInfo
            layerName = getCol(row, COL_LAYER_LABEL).toolTip()
            layerInfo = layerState.Proxy.getLayerInfoByLayerName(layerName)

            # get the layer
            layer = doc.getObject(layerInfo.NameLayer)

            visibility = getCol(row, COL_VISIBILITY).checkState() == \
                QtCore.Qt.Checked
            if visibility != layerInfo.Visible:
                checkChanged()
                layerInfo.Visible = visibility

            layerLabel = getCol(row, COL_LAYER_LABEL).text()
            if layerLabel:
                if layer.Label != layerLabel:
                    checkChanged()
                    layer.Label = layerLabel

            lineWidth = getCol(row, COL_LINE_WIDTH).\
                data(QtCore.Qt.DisplayRole)
            if lineWidth:
                if layerInfo.LineWidth != lineWidth:
                    checkChanged()
                    layerInfo.LineWidth = lineWidth

            drawStyle = getCol(row, COL_DRAW_STYLE).text()
            if drawStyle:
                if layerInfo.DrawStyle != drawStyle:
                    checkChanged()
                    layerInfo.DrawStyle = drawStyle

            lineColor = getCol(row, COL_LINE_COLOR).\
                data(QtCore.Qt.UserRole)
            if lineColor:
                if layerInfo.LineColor[3:] != lineColor:
                    checkChanged()
                    layerInfo.LineColor = lineColor

            shapeColor = getCol(row, COL_SHAPE_COLOR).\
                data(QtCore.Qt.UserRole)
            if shapeColor:
                if layerInfo.ShapeColor[3:] != shapeColor:
                    checkChanged()
                    layerInfo.ShapeColor = shapeColor

            transparency = getCol(row, COL_TRANSPARENCY).\
                data(QtCore.Qt.DisplayRole)
            if transparency:
                if layerInfo.Transparency != transparency:
                    checkChanged
                    layerInfo.Transparency = transparency

            linePrintColor = getCol(row, COL_LINE_PRINT_COLOR).\
                data(QtCore.Qt.UserRole)
            if linePrintColor:
                if layerInfo.LinePrintColor[3:] != linePrintColor:
                    checkChanged()
                    layerInfo.LinePrintColor = linePrintColor

        # recompute
        if changed:
            App.ActiveDocument.commitTransaction()
            App.ActiveDocument.recompute()

    def reject(self):
        """when Cancel button is pressed or dialog is closed"""

        # save dialog size
        pref = App.ParamGet("User parameter:BaseApp/Preferences/Mod/"
                            "OSHAutoDocWorkbench")
        pref.SetInt(PREF_WIDTH, self.dialog.width())
        pref.SetInt(PREF_HEIGHT, self.dialog.height())

        return True

    def addLayerState(self, obj):
        """Add a layer state to the list widget"""
        d('addLayerState')
        listWidget = self.dialog.listWidget
        listWidget.addItem(obj.Label)

    def updateLayerInfos(self, layerStateObjs):
        d('updateLayerInfos')
        for lsObj in layerStateObjs:
            lsObj.Proxy.updateLayerInfos()

    def updateDialog(self):
        """Update the dialog.

        The layer states are filled and the headers for the layer information.
        The layer info itself is not shown yet until a specific layer state is
        selected.
        """

        d('updateDialog')
        self.model.clear()

        # set header
        self.model.setHorizontalHeaderLabels([translate("", "On"),
                                              translate("", "Name"),
                                              translate("", "Line width"),
                                              translate("", "Draw style"),
                                              translate("", "Line color"),
                                              translate("", "Face color"),
                                              translate("", "Transparency"),
                                              translate("",
                                                        "Line print color")])
        self.dialog.tree.header().setDefaultSectionSize(72)
        self.dialog.tree.setColumnWidth(0, 32)  # on/off column
        self.dialog.tree.setColumnWidth(COL_LAYER_LABEL, 128)  # name column

        objs = findObjectsByType(LayerState)
        self.updateLayerInfos(objs)
        objs.sort(key=lambda o: o.Label)
        for obj in objs:
            self.addLayerState(obj)

    def updateLayers(self, layerState):
        "rebuild the model from document contents"
        doc = App.ActiveDocument
        d('updateLayers')

        self.model.setRowCount(0)

        # populate the layer info part with layerinfos
        for li in sorted(layerState.Group,
                         key=lambda li: doc.getObject(li.NameLayer).Label):
            self.addLayerRow(li)

    def addLayerRow(self, layerInfo):
        "adds a row to the model"

        doc = App.ActiveDocument

        from PySide import QtCore, QtGui
        # create row with default values
        onItem = QtGui.QStandardItem()
        onItem.setCheckable(True)
        onItem.setCheckState(QtCore.Qt.Checked if layerInfo.Visible
                             else QtCore.Qt.Unchecked)

        layer = doc.getObject(layerInfo.NameLayer)
        nameItem = QtGui.QStandardItem(layer.Label)
        nameItem.setToolTip(layerInfo.NameLayer)

        widthItem = QtGui.QStandardItem()
        widthItem.setData(layerInfo.LineWidth, QtCore.Qt.DisplayRole)

        styleItem = QtGui.QStandardItem(layerInfo.DrawStyle)

        lineColorItem = QtGui.QStandardItem()
        lineColorItem.setData(layerInfo.LineColor[:3], QtCore.Qt.UserRole)

        shapeColorItem = QtGui.QStandardItem()
        shapeColorItem.setData(layerInfo.ShapeColor[:3], QtCore.Qt.UserRole)

        transparencyItem = QtGui.QStandardItem()
        transparencyItem.setData(layerInfo.Transparency, QtCore.Qt.DisplayRole)

        linePrintColorItem = QtGui.QStandardItem()
        linePrintColorItem.setData(layerInfo.LinePrintColor[:3],
                                   QtCore.Qt.UserRole)

        lineColorItem.setIcon(getColorIcon(lineColorItem.data(
            QtCore.Qt.UserRole)))
        shapeColorItem.setIcon(getColorIcon(shapeColorItem.data(
            QtCore.Qt.UserRole)))
        linePrintColorItem.setIcon(getColorIcon(linePrintColorItem.data(
            QtCore.Qt.UserRole)))

        # append row
        self.model.appendRow([onItem,
                              nameItem,
                              widthItem,
                              styleItem,
                              lineColorItem,
                              shapeColorItem,
                              transparencyItem,
                              linePrintColorItem])

    def getPref(self, value, default, valuetype="Unsigned"):

        "retrieves a view pref value"

        p = App.ParamGet("User parameter:BaseApp/Preferences/View")
        if valuetype == "Unsigned":
            c = p.GetUnsigned(value, default)
            r = float((c >> 24) & 0xFF)/255.0
            g = float((c >> 16) & 0xFF)/255.0
            b = float((c >> 8) & 0xFF)/255.0
            return (r, g, b,)
        elif valuetype == "Integer":
            return p.GetInt(value, default)

    def onToggle(self):

        "toggle selected layers on/off"

        from PySide import QtCore
        state = None
        for index in self.dialog.tree.selectedIndexes():
            if index.column() == COL_VISIBILITY:
                # get state from first selected row
                if state is None:
                    if self.model.itemFromIndex(index).checkState() == \
                       QtCore.Qt.Checked:
                        state = QtCore.Qt.Unchecked
                    else:
                        state = QtCore.Qt.Checked
                self.model.itemFromIndex(index).setCheckState(state)

    def onIsolate(self):

        "isolates the selected layers (turns all the others off"

        from PySide import QtCore
        onrows = []
        for index in self.dialog.tree.selectedIndexes():
            if not index.row() in onrows:
                onrows.append(index.row())
        for row in range(self.model.rowCount()):
            if row not in onrows:
                self.model.item(row, COL_VISIBILITY).\
                    setCheckState(QtCore.Qt.Unchecked)

    def setLineColor(self, r, g, b):
        "change the line color of selected layers"
        for index in self.dialog.tree.selectedIndexes():
            if index.column() == COL_LINE_COLOR:
                color = QtGui.QColor(r, g, b).getRgbF()
                self.model.itemFromIndex(index).setData(color[:3],
                                                        QtCore.Qt.UserRole)
                self.model.itemFromIndex(index).setIcon(
                    getColorIcon(color))

    def onBlack(self):
        "turn the line color of selected layers black"
        self.setLineColor(25, 25, 25)

    def onGrey(self):
        "turn the line color of selected layers grey"
        self.setLineColor(188, 190, 192)


if App.GuiUp:

    from PySide import QtCore, QtGui

    class LayerStateManagerDelegate(QtGui.QStyledItemDelegate):

        "model delegate"

        def __init__(self, parent=None, *args):

            QtGui.QStyledItemDelegate.__init__(self, parent, *args)
            # setEditorData() is triggered several times.
            # But we want to show the color dialog only the first time
            self.first = True

        def createEditor(self, parent, option, index):

            if index.column() == COL_VISIBILITY:
                editor = QtGui.QCheckBox(parent)
            if index.column() == COL_LAYER_LABEL:
                editor = QtGui.QLineEdit(parent)
                editor.setEnabled(False)
            elif index.column() == COL_LINE_WIDTH:
                editor = QtGui.QSpinBox(parent)
                editor.setMaximum(99)
            elif index.column() == COL_DRAW_STYLE:
                editor = QtGui.QComboBox(parent)
                editor.addItems(["Solid", "Dashed", "Dotted", "Dashdot"])
            elif index.column() == COL_LINE_COLOR:
                editor = QtGui.QLineEdit(parent)
                self.first = True
            elif index.column() == COL_SHAPE_COLOR:
                editor = QtGui.QLineEdit(parent)
                self.first = True
            elif index.column() == COL_TRANSPARENCY:
                editor = QtGui.QSpinBox(parent)
                editor.setMaximum(100)
            elif index.column() == COL_LINE_PRINT_COLOR:
                editor = QtGui.QLineEdit(parent)
                self.first = True
            return editor

        def setEditorData(self, editor, index):

            if index.column() == COL_VISIBILITY:
                editor.setChecked(index.data())
            elif index.column() == COL_LAYER_LABEL:
                editor.setText(index.data())
            elif index.column() == COL_LINE_WIDTH:
                editor.setValue(index.data())
            elif index.column() == COL_DRAW_STYLE:
                editor.setCurrentIndex(["Solid", "Dashed", "Dotted",
                                        "Dashdot"].index(index.data()))
            elif index.column() == COL_LINE_COLOR:
                editor.setText(str(index.data(QtCore.Qt.UserRole)))
                if self.first:
                    c = index.data(QtCore.Qt.UserRole)
                    color = QtGui.QColorDialog.getColor(QtGui.QColor(
                        int(c[0]*255), int(c[1]*255), int(c[2]*255)))
                    editor.setText(str(color.getRgbF()))
                    self.first = False
            elif index.column() == COL_SHAPE_COLOR:
                editor.setText(str(index.data(QtCore.Qt.UserRole)))
                if self.first:
                    c = index.data(QtCore.Qt.UserRole)
                    color = QtGui.QColorDialog.getColor(QtGui.QColor(
                        int(c[0]*255), int(c[1]*255), int(c[2]*255)))
                    editor.setText(str(color.getRgbF()))
                    self.first = False
            elif index.column() == COL_TRANSPARENCY:
                editor.setValue(index.data())
            elif index.column() == COL_LINE_PRINT_COLOR:
                editor.setText(str(index.data(QtCore.Qt.UserRole)))
                if self.first:
                    c = index.data(QtCore.Qt.UserRole)
                    color = QtGui.QColorDialog.getColor(QtGui.QColor(
                        int(c[0]*255), int(c[1]*255), int(c[2]*255)))
                    editor.setText(str(color.getRgbF()))
                    self.first = False

        def setModelData(self, editor, model, index):

            if index.column() == COL_VISIBILITY:
                model.setData(index, editor.isChecked())
            elif index.column() == COL_LAYER_LABEL:
                model.setData(index, editor.text())
            elif index.column() == COL_LINE_WIDTH:
                model.setData(index, editor.value())
            elif index.column() == COL_DRAW_STYLE:
                model.setData(index, ["Solid", "Dashed", "Dotted",
                                      "Dashdot"][editor.currentIndex()])
            elif index.column() == COL_LINE_COLOR:
                model.setData(index, eval(editor.text()), QtCore.Qt.UserRole)
                model.itemFromIndex(index).setIcon(
                    getColorIcon(eval(editor.text())))
            elif index.column() == COL_SHAPE_COLOR:
                model.setData(index, eval(editor.text()), QtCore.Qt.UserRole)
                model.itemFromIndex(index).setIcon(getColorIcon(
                    eval(editor.text())))
            elif index.column() == COL_TRANSPARENCY:
                model.setData(index, editor.value())
            elif index.column() == COL_LINE_PRINT_COLOR:
                model.setData(index, eval(editor.text()), QtCore.Qt.UserRole)
                model.itemFromIndex(index).setIcon(getColorIcon(
                    eval(editor.text())))

# In the FreeCAD console, you can access to this class in the following way:
# import freecad.OSHAutoDocWorkbench.layer_state_manager as lsm
# lsm = lsm.lsm
lsm = LayerStateManager()
Gui.addCommand(LayerStateManager.command_name, lsm)
