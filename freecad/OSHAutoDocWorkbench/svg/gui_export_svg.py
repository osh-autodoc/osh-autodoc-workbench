# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
import math

import FreeCADGui as Gui
import FreeCAD as App

import pivy.coin as coin
import PySide.QtGui as QtGui

import DraftVecUtils


from .. import ICONPATH
from . import svg

import draftguitools.gui_base as gui_base
import Draft

from ..util.debug import logDebug
from ..util.debug import d


def getCenterPoint(x, y, z):
    """Get the center point."""

    v = App.Vector(x, y, z)
    view = Gui.ActiveDocument.ActiveView
    camera = view.getCameraNode()
    cam1 = App.Vector(camera.position.getValue().getValue())
    cam2 = Gui.ActiveDocument.ActiveView.getViewDirection()
    vcam1 = DraftVecUtils.project(cam1, v)

    a = vcam1.getAngle(cam2)
    if a < 0.0001:
        return App.Vector()
    _d = vcam1.Length
    L = _d/math.cos(a)

    vcam2 = DraftVecUtils.scaleTo(cam2, L)

    cp = cam1.add(vcam2)

    return cp


def exportSVG(filename, objects):

    doc = App.ActiveDocument
    doc.openTransaction("Create SVG")
    Gui.addModule("Draft")

    # from draft
    view = Gui.ActiveDocument.ActiveView
    dir = view.getViewDirection().negative()
    # d("dir", dir)
    camera = view.getCameraNode()
    rot = camera.getField("orientation").getValue()
    coin_up = coin.SbVec3f(0, 1, 0)
    upvec = App.Vector(rot.multVec(coin_up).getValue())
    # d("upvec", upvec)
    cp = getCenterPoint(dir.x, dir.y, dir.z)

    xdir = upvec.cross(dir)
    # d("xdir", xdir)

    _cmd = "FreeCAD.DraftWorkingPlane.alignToPointAndAxis"
    _cmd += "("
    _cmd += tostr(cp) + ", "
    _cmd += tostr((dir.x, dir.y, dir.z)) + ", "
    _cmd += "0.0, "
    _cmd += tostr(upvec)
    _cmd += ")"
    Gui.doCommandGui(_cmd)

    # Gui.doCommand('FreeCAD.ActiveDocument.recompute()')
    # Gui.Snapper.restack()
    # Gui.Snapper.show()

    viewer = view.getViewer()
    rm = viewer.getSoRenderManager()
    vp = rm.getViewportRegion()
    # sz = vp.getWindowSize().getValue()
    # w = sz[0]
    # h = sz[1]

    sp = vp.getViewportSizePixels().getValue()
    op = vp.getViewportOriginPixels().getValue()
    siz = vp.getViewportSize().getValue()
    dX = siz[0]
    dY = siz[1]
    fRatio = vp.getViewportAspectRatio()

    def convert(coor):
        locX = coor[0] - op[0]
        locY = coor[1] - op[1]
        pX = locX / sp[0]
        pY = locY / sp[1]

        if fRatio > 1.0:
            pX = (pX - 0.5 * dX) * fRatio + 0.5 * dX
        elif fRatio < 1.0:
            pY = (pY - 0.5 * dY) / fRatio + 0.5 * dY
        return (pX, pY)

    # sizePixels = viewer.getSoRenderManager()\
    #                    .getViewportRegion()\
    #                    .getViewportSizePixels().getValue()

    # objects = viewer.getPickedList(pos=[convert((0, 0)),
    #                                     convert((sp[0], sp[1]))],
    #                                pickElement=False, mapCoords=False)

    upperLeft = viewer.getPoint(0, 0)
    upperRight = viewer.getPoint(sp[0], 0)
    bottomLeft = viewer.getPoint(0, sp[1])

    length = upperLeft.distanceToPoint(upperRight)
    height = upperLeft.distanceToPoint(bottomLeft)

    f = open(filename, "w")
    f.write("<svg\n")
    f.write(f"       width=\"{length}\"\n")
    f.write(f"       height=\"{height}\"\n")
    f.write("	xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"\n")
    f.write("	xmlns:freecad=\"http://www.freecadweb.org/wiki/index.php")
    f.write("?title=Svg_Namespace\">\n")
    f.write(svg.getSVG(xdir, bottomLeft, objects, cp,
                       length, height))
    f.write("\n</svg>")
    f.close()

    doc.commitTransaction()


# from draft
def tostr(v):
    """Make a string from a vector or tuple."""
    string = "FreeCAD.Vector("
    string += str(v[0]) + ", "
    string += str(v[1]) + ", "
    string += str(v[2]) + ")"
    return string


class ExportSVG(gui_base.GuiCommandSimplest):
    """GuiCommand to export an SVG of the current view."""

    command_name = 'OSHAutoDoc_ExportSVG'

    def __init__(self):
        super(ExportSVG, self).__init__(name="ExportSVG")
        self.dir = ''

    def GetResources(self):
        """Set icon, menu and tooltip."""
        return {'Pixmap': os.path.join(ICONPATH, 'export-svg.svg'),
                'MenuText': "Export SVG",
                'ToolTip': "Export an SVG based on the current view."}

    # From Draft

    def getMoveVectorEdge(self, edge):
        return edge.Vertexes[0].Point.sub(edge.Vertexes[1].Point).multiply(0.5)

    def getMoveVector(self, length, rectangle):
        """Get the vector to move the rectangle of-center"""
        edges = rectangle.Shape.Edges
        if edges[0].Length == length:
            return self.getMoveVectorEdge(edges[0])
        elif edges[1].Length == length:
            return self.getMoveVectorEdge(edges[1])
        else:
            raise "Can't find an edge with the right length"

    def Activated(self):
        """Execute when the command is called.
        """
        super(ExportSVG, self).Activated()

        if self.dir:
            filename = os.path.join(self.dir, "current-view.svg")
        else:
            filename = "current-view.svg"

        qFileDialog = QtGui.QFileDialog
        filepattern = qFileDialog.getSaveFileName(caption="Save SVG file",
                                                  dir=filename,
                                                  filter="SVG images (*.svg)")
        file = filepattern[0]
        exportSVG(file)

Gui.addCommand(ExportSVG.command_name, ExportSVG())
