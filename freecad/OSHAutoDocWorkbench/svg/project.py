# SPDX-FileCopyrightText: 2023 Pieter Hijma <pieter@hiww.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import TechDraw as td
import Part


def getSVGForKind(compound, style):
    style.setdefault('stroke', 'rgb(0, 0, 0)')
    style.setdefault('stroke-width', '1.0')
    style.setdefault('stroke-linecap', 'round')
    style.setdefault('stroke-linejoin', 'round')
    style.setdefault('fill', 'none')
    style.setdefault('transform', 'scale(1,-1)')

    svg = ''
    if not compound.isNull():
        svg += '<g'
        for k, v in style.items():
            svg += f' {k} = \"{v}\"\n'
        svg += '>\n'
        svg += td.exportSVGEdges(compound)
        svg += '</g>\n'
    return svg


def getSVG(projShapes, shape, vStyle, vOStyle, v1Style):
    v = td.build3dCurves(projShapes.vCompound(shape))
    vO = td.build3dCurves(projShapes.outLineVCompound(shape))

    svg = getSVGForKind(v, vStyle)
    svg += getSVGForKind(vO, vOStyle)

    return svg


def projectToSVG(shapes, origin, direction, xDirection,
                 hStyle, h0Style, h1Style, vStyles, v0Styles, v1Style):
    algo = Part.HLRBRep.Algo()
    for shape in shapes:
        algo.add(shape)
    algo.setProjector(origin, direction, xDirection)
    algo.update()
    algo.hide()

    svg = ''
    projShapes = Part.HLRBRep.HLRToShape(algo)
    for shape, vStyle, v0Style in zip(shapes, vStyles, v0Styles):
        svg += getSVG(projShapes, shape, vStyle, v0Style, v1Style)

    return svg
