# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD
import Draft

from . import project
# from ..util.debug import d

# this one is needed for setting up a FreeCAD.DraftWorkingPlane
import DraftTools


def getPlacement(cp):
    p = FreeCAD.Placement(FreeCAD.DraftWorkingPlane.getPlacement())
    p.Base = cp

    return p


def isPosition(o):
    """Whether an object is a position object"""
    return hasattr(o, 'Proxy') and hasattr(o.Proxy, 'Type') and \
        o.Proxy.Type == "Position"


def looksLikeDraft(o):

    """Does this object look like a Draft shape? (flat,  no solid,  etc)"""

    # If there is no shape at all ignore it
    if not hasattr(o,  'Shape') or o.Shape.isNull():
        return False
    # If there are solids in the object,  it will be handled later
    # by getCutShapes
    if len(o.Shape.Solids) > 0:
        return False
    # If we have a shape,  but no volume,  it looks like a flat 2D object
    return o.Shape.Volume < 0.0000001  # add a little tolerance...


def getShapes(objs):
    # print("Do I get shapes from objs?")
    # print(f"objs: {objs}\n")
    shapes = []
    newobjs = []
    for o in objs:
        if hasattr(o, 'Shape'):
            if o.Shape.isNull():
                pass
            elif o.Shape.isValid():
                # shapes.extend(o.Shape.Solids)
                # probably this one is better:
                shapes.append(o.Shape)
                newobjs.append(o)
            else:
                print(f"{o.Name} is invalid")
    # print(f"shapes: {shapes}\n")
    return (newobjs, shapes)


def is2DWithSurface(o):
    "Whether an object is 2D with a surface"
    return hasattr(o, "ViewObject") and \
        o.isDerivedFrom("Part::Part2DObject") and \
        o.Area.Value > 0.0


def getSVGBOM(obj, filename):
    import Draft
    lineColor = (0.0, 0.0, 0.0)

    import FreeCAD as App
    import FreeCADGui as Gui
    import Part
    import pivy.coin as coin

    from . import gui_export_svg as ge
    view = Gui.ActiveDocument.ActiveView
    dir = view.getViewDirection().negative()
    camera = view.getCameraNode()
    rot = camera.getField("orientation").getValue()
    coin_up = coin.SbVec3f(0, 1, 0)
    upvec = App.Vector(rot.multVec(coin_up).getValue())
    cp = ge.getCenterPoint(dir.x, dir.y, dir.z)
    xdir = upvec.cross(dir)

    doc = App.ActiveDocument
    shape = obj.Shape
    origin = cp
    direction = dir
    xDirection = xdir
    # lineColor = (0.0, 0.0, 0.0)

    algo = Part.HLRBRep.Algo()
    algo.add(shape)
    algo.setProjector(origin, direction, xDirection)
    algo.update()
    algo.hide()
    projShapes = Part.HLRBRep.HLRToShape(algo)

    import TechDraw as td
    v = td.build3dCurves(projShapes.vCompound(shape))
    vO = td.build3dCurves(projShapes.outLineVCompound(shape))
    bb = v.BoundBox

    f = open(filename, "w")
    f.write("<svg\n")
    f.write(f"       width=\"{bb.XLength}\"\n")
    f.write(f"       height=\"{bb.YLength}\"\n")
    f.write(f"       viewBox=\"0 0 {bb.XLength} {bb.YLength}\"\n")
    f.write("	xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"\n")
    f.write("	xmlns:freecad=\"http://www.freecadweb.org/wiki/index.php")
    f.write("?title=Svg_Namespace\">\n")
    f.write('<g stroke=\"#000000\"\n')
    f.write('   stroke-width=\"0.4px\"\n')
    f.write('   stroke-linecap=\"round\"\n')
    f.write('   stroke-linejoin=\"round\"\n')
    f.write('   fill=\"none\"\n')
    f.write(f'   transform=\"matrix(1, 0, 0, -1, {-bb.XMin}, {bb.YMax})\">\n')
    f.write(td.exportSVGEdges(v) + '\n' + td.exportSVGEdges(vO))
    f.write("\n</g>")
    f.write("\n</svg>")
    f.close()


def getSVG(xdir, topleft2, objects, cp, length, height, placement=''):
    """
    Return an SVG fragment

    """
    if not placement:
        placement = getPlacement(cp)
    direction = placement.Rotation.multVec(FreeCAD.Vector(0,  0,  1))

    # print(f"{len(objects)} objects")
    # for (i, o) in enumerate(objects):
    #     print(f'{i}: {o.Name}')

    # separate spaces and Draft objects
    spaces = []
    nonspaces = set()
    positioned = set()
    for o in objects:
        if isPosition(o):
            # if an object is a position, remember the object to position to it
            # out later, the position object itself is added
            positioned.add(o.ObjectToPosition)
            nonspaces.add(o)
            if o.Line:
                nonspaces.add(o.Line)
        elif Draft.getType(o) == "Space":
            spaces.append(o)
        elif o.isDerivedFrom("Part::Part2DObject"):
            # 2d objects (draft) are processed similarly
            nonspaces.add(o)
        elif o.isDerivedFrom("App::DocumentObjectGroup"):
            # These will have been expanded by getSectionData already
            # Don't know what to do here exactly Arch has the above comment
            # (ArchSectionPlane.py)
            pass
        elif looksLikeDraft(o):
            # 2d objects (draft) are processed similarly
            nonspaces.add(o)
        else:
            nonspaces.add(o)

    # the objects to show are the non space ones bare the object that are
    # positioned.
    objs = list(nonspaces.difference(positioned))

    # print(f'objs: {objs}')

    objs, vshapes = getShapes(objs)
    # for (i, vs) in enumerate(vshapes):
    #     print(f'{i}: {vs}')

    # print(f"After filtering: {len(objs)} objects")
    # for (i, o) in enumerate(objs):
    #     print(f'{i}: {o.Name}')

    # determine the line color of the objects, default to black
    lineColor = (0.0, 0.0, 0.0)
    lineColors = []
    for x in objs:
        if hasattr(x.ViewObject, "LineColor"):
            lineColors.append(x.ViewObject.LineColor)
        else:
            lineColors.append((0.0, 0.0, 0.0, 0.0))

    # print(f"linecolors: {len(lineColors)} objects")
    # for (i, lc) in enumerate(lineColors):
    #     print(f'{i}: {lc}')


            
    # determine the shape color of the objects, default to "none"
    # 2D elements with a surface should be colored to their shape color
    fillColors = [Draft.getrgb(x.ViewObject.ShapeColor) if is2DWithSurface(x)
                  else "none" for x in objs]

    # print(f"fillcolors: {len(fillColors)} objects")
    # for (i, fc) in enumerate(fillColors):
    #     print(f'{i}: {fc}')
    # print(f"lineColors: {lineColors}")
    # print(f"fillColors: {fillColors}")

    svgLineWidth = '0.4px'
    svgLineColor = Draft.getrgb(lineColor)
    svg = ''

    translateX = 0.0
    translateY = 0.0

    import WorkingPlane
    wp = WorkingPlane.plane()
    wp.setFromPlacement(placement)

    # print(f"tl2: {topleft2}")
    # print(f"tll2: {wp.getLocalCoords(topleft2)}")

    translateVector = wp.getLocalCoords(topleft2)

    translateX = -translateVector.x
    translateY = -translateVector.y


    # d("direction", direction)
    # d("translateX", translateX)
    # d("translateY", translateY)
    # d("xdir", xdir)
    # d("placement", placement)

    # render using the Projection module
    # import Projection
    if vshapes:
        style = {'stroke':       f"{svgLineColor}",
                 'stroke-width': f"{svgLineWidth}"}
        styles = [{'stroke': Draft.getrgb(lineColor),
                   'stroke-width': f"{svgLineWidth}",
                   'fill': fillColor,
                   'transform':
                   f"scale(1,-1) translate({translateX}, {translateY})"}
                  for lineColor, fillColor in zip(lineColors, fillColors)]
        # svg += Projection.projectToSVG2(
        #     vshapes, FreeCAD.Vector(0, 0, 0), direction, xdir,
        #     hStyle=style, h0Style=style, h1Style=style,
        #     vStyles=styles, v0Styles=styles, v1Style=style)
        svg += project.projectToSVG(
            vshapes, FreeCAD.Vector(0, 0, 0), direction, xdir,
            hStyle=style, h0Style=style, h1Style=style,
            vStyles=styles, v0Styles=styles, v1Style=style)
    return svg
