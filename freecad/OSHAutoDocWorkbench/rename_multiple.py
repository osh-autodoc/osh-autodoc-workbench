# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

import FreeCAD as App
import FreeCADGui as Gui

from . import ICONPATH


class RenameMultiple(object):
    """
    A command to rename multiple objects
    """

    command_name = 'OSHAutoDoc_RenameMultiple'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        return {'Pixmap': os.path.join(ICONPATH, 'labels.svg'),
                'MenuText': 'Rename multiple objects',
                'ToolTip': 'Select multiple objects to rename'}

    def Activated(self):
        """
        Show the RenameMultipleDialog on activating this command.
        """

        dialog = Gui.PySideUic.loadUi(
            os.path.join(ICONPATH, 'RenameMultipleDialog.ui'))

        result = dialog.exec()
        if result == dialog.Accepted:
            labelName = dialog.labelName.text()
            if labelName != "":
                # control how we count
                # this is especially useful for labels that end on a number,
                # because FreeCAD will automatically increment these numbers.
                # by adding a space and "001" we control how the objects are
                # numbered.
                labelName += " 001"
                for i in Gui.Selection.getCompleteSelection():
                    i.Label = labelName


# Add the command to the system
Gui.addCommand(RenameMultiple.command_name, RenameMultiple())
