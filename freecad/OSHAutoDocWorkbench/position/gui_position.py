# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

import FreeCADGui as Gui

from .. import ICONPATH

import draftguitools.gui_base as gui_base

from . import make_position as mp


class Position(gui_base.GuiCommandSimplest):
    """GuiCommand to create Position objects in the document."""

    command_name = 'OSHAutoDoc_Position'

    def __init__(self):
        super(Position, self).__init__(name="Position")

    def GetResources(self):
        """Set icon, menu and tooltip."""
        return {'Pixmap': os.path.join(ICONPATH, 'position-icon.svg'),
                'MenuText': "Add position(s)",
                'ToolTip': "First select a face normal to the new positions, "
                "then select (subshapes of) objects that need to be "
                "repositioned."}

    def Activated(self):
        """Execute when the command is called.

        It calls the `finish(False)` method of the active Draft command.
        """
        super(Position, self).Activated()

        self.doc.openTransaction("Create Position")
        Gui.addModule("freecad.OSHAutoDocWorkbench")
        Gui.doCommand(mp.command_make_positions)
        Gui.doCommand('FreeCAD.ActiveDocument.recompute()')
        self.doc.commitTransaction()


class PositionScrew(gui_base.GuiCommandSimplest):
    """GuiCommand to create Position objects based on screws."""

    command_name = 'OSHAutoDoc_PositionScrew'

    def __init__(self):
        super(PositionScrew, self).__init__(name="PositionScrew")

    def GetResources(self):
        """Set icon, menu and tooltip."""
        return {'Pixmap': os.path.join(ICONPATH, 'position-screw-icon.svg'),
                'MenuText': "Add position(s) for screws",
                'ToolTip': "Select screws to reposition"}

    def Activated(self):
        """Execute when the command is called.

        It calls the `finish(False)` method of the active Draft command.
        """
        super(PositionScrew, self).Activated()

        self.doc.openTransaction("Create Position for screws")
        Gui.addModule("freecad.OSHAutoDocWorkbench")
        Gui.doCommand(mp.command_make_positions_screws)
        Gui.doCommand('FreeCAD.ActiveDocument.recompute()')
        self.doc.commitTransaction()


Gui.addCommand(Position.command_name, Position())
Gui.addCommand(PositionScrew.command_name, PositionScrew())
