# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

from pivy import coin

import FreeCAD as App
import FreeCADGui as Gui

from draftobjects.wire import Wire

from .. import ICONPATH
# from .position import Position


class ViewProviderPosition:
    """The viewprovider for the Position object."""

    def __init__(self, vobj):
        self.Object = vobj.Object
        self.parents = []
        vobj.Proxy = self

    def getIcon(self):
        """Return the path to the icon used by the viewprovider.
        """
        return os.path.join(ICONPATH, "position-icon.svg")

    def attach(self, vobj):
        """Set up the scene sub-graph of the viewprovider."""

        self.Object = vobj.Object
        # Not necessary because this view provider has a shape
        pass

    def claimChildren(self):
        """Return objects that will be placed under it in the tree view.
        """

        obj = self.Object
        if obj.Line:
            return [obj.Line]
        else:
            return []

    # The following methods are not required since we have a shape and it is
    # clear what kind of object we are.

    # def getDisplayModes(self, vobj):
    #     """Return the display modes that this viewprovider supports."""

    # def getDefaultDisplayMode(self):
    #     """Return the default display mode."""

    # def setDisplayMode(self, mode):
    #     """Return the saved display mode."""

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return None

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        self.parents = []
        return None

    def updateData(self, obj, prop):
        """Execute when a property from the Proxy class is changed."""
        pass

    def change_view_properties(self, vobj, prop):
        """Iterate over the contents and change the properties."""
        pass

    def removeFromSceneGraph(self, vobj):
        rootNode = vobj.RootNode
        sg = Gui.ActiveDocument.ActiveView.getSceneGraph()
        
        sa = coin.SoSearchAction()
        sa.setNode(vobj.RootNode)
        sa.setInterest(coin.SoSearchAction.ALL)
        sa.apply(sg)
        paths = sa.getPaths()
        if paths:
            self.parents = [path.getNodeFromTail(1) for path in paths]
        for parent in self.parents:
            parent.removeChild(rootNode)

    def addToSceneGraph(self, vobj):
        rootNode = vobj.RootNode
        if self.parents:
            for parent in self.parents:
                parent.addChild(rootNode)
        else:
            sg = Gui.ActiveDocument.ActiveView.getSceneGraph()
            sg.addChild(rootNode)

    def changeVisibility(self, visible, vobj):
        """Change the visibility of this object.

        The line changes with the visibility of this object.  The visibility of
        the original object (the object to (re)position) is not changed to make
        sure that we don't depend on its visibility.  Instead, we add or remove
        the object to position from the scene graph.
        """

        obj = vobj.Object
        objToPosition = obj.ObjectToPosition
        if objToPosition:
            vobjToPosition = objToPosition.ViewObject
            if visible:
                self.removeFromSceneGraph(vobjToPosition)
            else:
                self.addToSceneGraph(vobjToPosition)

        line = obj.Line
        if line:
            line.ViewObject.Visibility = visible

    def onChanged(self, vobj, prop):
        """Execute when a view property is changed."""

        if prop == "Visibility":
            self.changeVisibility(vobj.Visibility, vobj)

    def canDragObject(self, obj):
        """Whether dragging object obj from the position is allowed."""
        return True

    def canDragObjects(self):
        """Whether dragging objects from the position is allowed."""
        return True

    def dragObject(self, vobj, otherobj):
        """Remove the object that was dragged from the position."""
        if otherobj == vobj.Object.Line:
            vobj.Object.Line = None

            App.ActiveDocument.recompute()

    def canDropObject(self, obj):
        """Return true to allow dropping object obj."""
        return hasattr(obj, "Proxy") and isinstance(obj.Proxy, Wire)

    def canDropObjects(self):
        """Return true to allow dropping objects."""
        return True

    def dropObject(self, vobj, otherobj):
        """Add object that was dropped into the Layer State to the group.

        Only wires are allowed.
        """

        obj = vobj.Object
        obj.Line = otherobj

        App.ActiveDocument.recompute()

    def setupContextMenu(self, vobj, menu):
        """Set up actions to perform in the context menu."""
        pass
