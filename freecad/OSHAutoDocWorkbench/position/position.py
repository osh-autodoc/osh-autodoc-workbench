# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD as App

from Draft import makeWire

PROP_OBJ_TO_POSITION = "ObjectToPosition"
PROP_PLACEMENT = "Placement"
PROP_LINE = "Line"
PROP_AUTOMATIC_LINE_START = "AutomaticLineStart"
PROP_DISTANCE = "Distance"

# The #mm that we find that floats are equal
EPSILON = 0.01

# The default distance for automatic lines
DEFAULT_DISTANCE = 60

LINE_COLOR = (0.93, 0.13, 0.14)
LINE_WIDTH = 3.0

def eq(float1, float2):
    """Whether two floats are equal in terms of an esilon"""
    return abs(float1 - float2) < EPSILON


class Position:
    """The Position object.

    This object contains a new position for another position that we call the
    "ObjectToPosition", the object that we need to (re)position.
    """

    def __init__(self, obj):
        self.Type = "Position"
        self.Object = obj
        obj.Proxy = self

        self.set_properties(obj)

    def onDocumentRestored(self, obj):
        self.Object = obj

    def set_properties(self, obj):
        """Set properties only if they don't exist."""

        def checkProperty(prop, typeProp, tip):
            if prop not in obj.PropertiesList:
                obj.addProperty(typeProp, prop, "Base", tip)
                return True
            return False

        checkProperty(PROP_OBJ_TO_POSITION, "App::PropertyLink",
                      "The object that is repositioned")

        checkProperty(PROP_PLACEMENT, "App::PropertyPlacement",
                      "The placmenent of this position")

        checkProperty(PROP_LINE, "App::PropertyLink",
                      "The line that shows the repositioning")

        checkProperty(PROP_AUTOMATIC_LINE_START, "App::PropertyBool",
                      "Whether the start of the line should be automatic")

        # checkProperty(PROP_AUTOMATIC_LINE_END, "App::PropertyBool",
        #               "Whether the end of the line should be automatic")

        checkProperty(PROP_DISTANCE, "App::PropertyLength",
                      "The distance between the repositioned object and "
                      "the original one")

    def __getstate__(self):
        """Return a tuple of objects to save or None."""
        return self.Type

    def __setstate__(self, state):
        """Set the internal properties from the restored state."""
        if state:
            self.Type = state

    def getCenterPointFaceBoundBox(self, boundBox, intersectionPoint):
        """Return the center of the face of the bounding box given an intersection
        point.

        Find the face of the bounding box that the intersection point is on.
        Return a vector that represents the center of this face.
        """

        center = boundBox.Center
        if eq(boundBox.XMin, intersectionPoint.x) or \
           eq(boundBox.XMax, intersectionPoint.x):
            center.x = intersectionPoint.x
        elif (eq(boundBox.YMin, intersectionPoint.y) or
              eq(boundBox.YMax, intersectionPoint.y)):
            center.y = intersectionPoint.y
        elif (eq(boundBox.ZMin, intersectionPoint.z) or
              eq(boundBox.ZMax, intersectionPoint.z)):
            center.z = intersectionPoint.z
        else:
            center = intersectionPoint

        return center

    def makeInitialLine(self, obj):
        """Make an initial wire"""
        fromObj = obj
        toObj = obj.ObjectToPosition

        fromBB = fromObj.Shape.BoundBox
        toBB = toObj.Shape.BoundBox
        lineVector = toBB.Center.sub(fromBB.Center)
        intersectionPoint = fromBB.getIntersectionPoint(fromBB.Center,
                                                        lineVector)
        centerPoint = self.getCenterPointFaceBoundBox(fromBB,
                                                      intersectionPoint)

        obj.Line = makeWire([centerPoint, centerPoint.add(lineVector)])

    # Update the start of the line.
    #
    # It is difficult to also update the end of the line automatically because
    # it is hard to determine where the line should end.  Only having a way to
    # update the end of the line basing it on an initial line for which it is
    # logical to determine the end of the line, gives you flexibility.  You can
    # simply transform the line and determine yourself where to end the line.
    def updateLineStart(self, obj):
        """Update the start of the line between the object to position and this
        position object.
        """

        if obj.AutomaticLineStart:
            if not obj.Line:
                self.makeInitialLine(obj)

            fromObj = obj
            end = obj.Line.Points[1]

            fromBB = fromObj.Shape.BoundBox
            lineVector = end.sub(fromBB.Center)
            intersectionPoint = fromBB.getIntersectionPoint(fromBB.Center,
                                                            lineVector)
            centerPoint = self.getCenterPointFaceBoundBox(fromBB,
                                                          intersectionPoint)
            # compensate for different placement of the line
            centerPoint = centerPoint.sub(obj.Line.Placement.Base)

            if not centerPoint.isEqual(obj.Line.Points[0], EPSILON):
                obj.Line.Points = [centerPoint] + obj.Line.Points[1:]

            if App.GuiUp:
                vo = obj.Line.ViewObject
                if vo.LineColor != LINE_COLOR:
                    vo.LineColor = LINE_COLOR
                if vo.LineWidth != LINE_WIDTH:
                    vo.LineWidth = LINE_WIDTH

    def updateDistance(self, obj):
        """Update the distance between the object to position and this
        position"""

        if not eq(self.oldDistance, obj.Distance) and obj.Distance > 0:
            objToPosition = obj.ObjectToPosition
            direction = obj.Placement.Base.sub(
                objToPosition.Placement.Base).normalize()

            # scale direction
            direction.multiply(obj.Distance)

            obj.Placement.Base = objToPosition.Placement.Base.add(direction)

    def onBeforeChange(self, obj, prop):
        """Handle things before a property change is made."""

        if prop == PROP_DISTANCE:
            self.oldDistance = obj.Distance

    def onChanged(self, obj, prop):
        """Handle things when a property change is made."""

        if prop == PROP_AUTOMATIC_LINE_START:
            self.updateLineStart(obj)

        if prop == PROP_DISTANCE:
            self.updateDistance(obj)
            self.updateLineStart(obj)

    def execute(self, obj):
        """Execute when the object is created or recomputed."""

        if obj.ObjectToPosition:
            objToPosition = obj.ObjectToPosition
            if obj.Shape != objToPosition.Shape:
                obj.Shape = objToPosition.Shape

            distance = obj.Placement.Base.distanceToPoint(
                objToPosition.Placement.Base)
            if distance != obj.Distance:
                obj.Distance = distance
