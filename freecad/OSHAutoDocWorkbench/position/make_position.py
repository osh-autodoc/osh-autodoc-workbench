# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD as App

from .position import (Position, DEFAULT_DISTANCE)

if App.GuiUp:
    from .view_position import ViewProviderPosition

    import FreeCADGui as Gui

command_make_positions = '_positions_ = ' + \
    'freecad.OSHAutoDocWorkbench.position.make_position.' + \
    'make_positions()'
command_make_positions_screws = '_positions_ = ' + \
    'freecad.OSHAutoDocWorkbench.position.make_position.' + \
    'make_positions_screws()'


def make_positions():
    """Create Position objects in the active document.

    If items are selected, then make positions for these items.
    Otherwise, create empty positions.
    """

    doc = App.ActiveDocument

    if not doc:
        App.Console.PrintError("No active document. Aborting.")
        return None

    sel = Gui.Selection.getSelection()

    if len(sel) > 0:
        return list(map(lambda obj: make_position(doc, obj), sel))
    else:
        return make_position(doc)


def make_position(doc, objToPosition=None):

    new_obj = doc.addObject("Part::FeaturePython", "Position")
    Position(new_obj)

    if objToPosition is not None:
        new_obj.Label = "Pos of " + objToPosition.Label
        new_obj.ObjectToPosition = objToPosition
        new_obj.Shape = objToPosition.Shape
        new_obj.Placement = objToPosition.Placement

    if App.GuiUp:
        ViewProviderPosition(new_obj.ViewObject)

    return new_obj


def get_pull_direction(obj):
    cog = obj.Shape.CenterOfGravity
    cbb = obj.Shape.BoundBox.Center

    return cog.sub(cbb).normalize()


def make_position_screw(doc, objToPosition):
    obj = make_position(doc, objToPosition)

    pullDirection = get_pull_direction(objToPosition)

    # scale pullDirection
    pullDirection.multiply(DEFAULT_DISTANCE)
    newPosition = obj.Placement.Base.add(pullDirection)
    obj.Placement.Base = newPosition
    obj.AutomaticLineStart = True

    return obj


def make_positions_screws():
    """Create Position objects based on screws.

    If items are selected, then make positions for these items.
    Otherwise, print an error
    """

    doc = App.ActiveDocument

    if not doc:
        App.Console.PrintError("No active document. Aborting.")
        return None

    sel = Gui.Selection.getSelection()

    if len(sel) > 0:
        return list(map(lambda obj: make_position_screw(doc, obj), sel))
    else:
        App.Console.PrintError("No objects were selected")
        return []
