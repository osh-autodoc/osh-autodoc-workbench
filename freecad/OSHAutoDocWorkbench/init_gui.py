# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
import FreeCADGui as Gui
import FreeCAD as App

from . import ICONPATH
from .step_select import StepSelect
# from .rename_multiple import RenameMultiple
from .bom.gui_bom import (CreateBom,
                          CreateBomCSV,
                          CreateBomCSVFromLayers,
                          CreateBomFromLayers,
                          ExportSVGFromBomCSV,
                          CopyCameraVectorsToClipboard,
                          CreateLayerStateTD,
                          MoveToSelection)

# from .layer_state.gui_layer_state import LayerState
from .position.gui_position import (Position, PositionScrew)
from .layer_state_manager import LayerStateManager
# from .svg.gui_export_svg import ExportSVG

# add layers button

# we need this import
import draftguitools.gui_layers
import TechDraw
import TechDrawGui

# from freecad.OSHAutoDocWorkbench.step_group.make_step_group \
#     import make_step_group


class WholeObjectSelectionObserver:
    # Observer for selection that filters the whole object instead of a
    # subobject

    def getSubObjName(self, doc, obj, sub):
        # get the subobject name that represents the whole object
        # based on the code from Assembly 4
        objList = App.getDocument(doc).getObject(obj).getSubObjectList(sub)
        # Build the name of the selected sub-object for multiple sub-assembly
        # levels
        subObjName = ''
        # App.Console.PrintMessage(f'objList: {objList}')
        # first look for the linked object of the selected entity:
        # Get linked object name that handles sub-sub-assembly
        for subObj in objList:
            if subObj.TypeId == 'App::Link':
                subObjName = subObjName + subObj.Name + '.'
        # if no App::Link found, let's look for other things:
        if subObjName == '':
            for subObj in objList:
                if (subObj.TypeId == 'App::Part' or
                        subObj.TypeId == 'PartDesign::Body' or
                        subObj.isDerivedFrom('Part::Feature')):
                    # the objList contains also the top-level object, don't
                    # count it twice
                    if subObj.Name != obj:
                        subObjName = subObjName + subObj.Name + '.'
        return subObjName

    def addSelection(self, doc, obj, sub, pnt):
        # Since both 3D view clicks and manual tree selection gets into the
        # same callback we will determine by clicked coordinates, for manual
        # tree selections the coordinates are (0,0,0)
        if pnt != (0, 0, 0):
            # 3D view click
            subObjName = self.getSubObjName(doc, obj, sub)
            # App.Console.PrintMessage(f'{obj}, {subObjName}\n')
            if subObjName != '':
                # Gui.Selection.clearSelection()
                # App.Console.PrintMessage(f'{obj}, {subObjName}\n')
                Gui.Selection.removeSelection(doc, obj, sub)
                Gui.Selection.addSelection(doc, obj, subObjName)
#    def setPreselection(self, doc, obj, sub):
#        subObjName = self.getSubObjName(doc, obj, sub)
#        if subObjName != '':
#            o = App.getDocument(doc).getObject(obj)
#            Gui.Selection.setPreselection(o, subObjName)


class OSHAutoDocWorkbench(Gui.Workbench):
    """
    The main class of the workbench that is initialized at GUI startup
    """

    MenuText = "OSHAutoDoc"
    ToolTip = "Workbench for assembly instructions"
    Icon = os.path.join(ICONPATH, "main-icon.svg")
    # remove all the things that are not part of the initial documentation of
    # the workbench
    bomToolBox = [CreateBom.command_name,
                  CreateBomCSV.command_name,
                  CreateBomCSVFromLayers.command_name,
                  ExportSVGFromBomCSV.command_name,
                  CopyCameraVectorsToClipboard.command_name,
                  CreateLayerStateTD.command_name,
                  MoveToSelection.command_name]
    toolbox = ["Draft_Layer",
               StepSelect.command_name,
               PositionScrew.command_name,
               Position.command_name,
               LayerStateManager.command_name,
               CreateBomFromLayers.command_name]
    # toolbox.extend(bomToolBox)
    # toolbox.extend([ExportSVG.command_name])

    def GetClassName(self):
        return "Gui::PythonWorkbench"

    def Initialize(self):
        """
        Called on activation of the workbench.
        This is the place to import all commands.
        """
        App.Console.PrintMessage("Activating OSHAutoDoc workbench\n")

        self.appendToolbar('OSHAutoDoc', self.toolbox)
        self.appendMenu('Documentaton', self.toolbox)

    def Activated(self):
        '''
        On switching to this workbench, this function is called.
        '''
        # self.selectionObserver = WholeObjectSelectionObserver()
        # Gui.Selection.addObserver(self.selectionObserver, 0)
        pass

    def Deactivated(self):
        '''
        On deactivation of this workbench, this function is called.
        '''
        # Gui.Selection.removeObserver(self.selectionObserver)
        pass


# Register this workbench in the GUI
Gui.addWorkbench(OSHAutoDocWorkbench())
