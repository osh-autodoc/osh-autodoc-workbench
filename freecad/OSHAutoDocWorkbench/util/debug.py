# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD as App

DEBUG = 1

def d(s, o):
    c = App.Console
    c.PrintMessage(f"{s}: {o}\n")

def logDebug(s):
    c = App.Console
    if DEBUG:
        c.PrintMessage(f"{s}\n")

def logMessage(s, t='m'):
    c = App.Console
    if t.casefold() == 'w':
        c.PrintWarning(f"{s}\n")
    elif t.casefold() == 'm':
        c.PrintMessage(f"{s}\n")
