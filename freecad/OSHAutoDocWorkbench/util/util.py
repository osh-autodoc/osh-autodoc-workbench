# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import FreeCAD as App


def findObjectsByType(the_type):
    doc = App.ActiveDocument
    if doc is None:
        return []
    else:
        return list(filter(lambda o: hasattr(o, 'Proxy') and
                           isinstance(o.Proxy, the_type),
                           App.ActiveDocument.Objects))


def renameFileName(fn):
    return fn.lower().replace(' ', '-').replace('---', '-')

def movePlacement(obj, objref):
    """
    moves obj to objref placement
    """
    originalPlacement = obj.Placement
    if not (isClassType(obj, "Part.Feature") or isClassType(objref, "Part.Feature")):
        print("Only part objects may be moved")
        return 1

    if hasattr(obj, "Placement") and hasattr(objref, "Placement"):
        obj.Placement = objref.Placement
    return originalPlacement

def getLinkedObject(obj):
    """
    returns derived linked obj
    eg.
    getLinkedObject(obj)
    """
    if obj.isDerivedFrom("App::Link"):
        return obj.LinkedObject
    return obj

def isClassType(obj, classType):
    """
    e.g.
    isClassType(obj, "Part.Feature")
    """
    classType = eval(classType)
    obj = getLinkedObject(obj)
    if isinstance(obj, classType):
        return True
    else:
        return False
