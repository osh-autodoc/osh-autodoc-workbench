# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc0x0b@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

# Global version number for this workbench
__version__ = "0.1.0"
