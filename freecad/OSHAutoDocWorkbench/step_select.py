# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

from PySide2 import QtGui
from PySide2.QtCore import Qt

import FreeCAD as App
import FreeCADGui as Gui

from draftobjects.layer import Layer
from FreeCAD import ReturnType

from . import ICONPATH
from .util.util import findObjectsByType




class StepSelectPanel:
    """
    Class for the panel for selecting parts into a step.
    """

    def getLayerContainer(self):
        doc = App.ActiveDocument
        return doc.getObject('LayerContainer')

    def __init__(self):
        """
        Construct the panel for selecting parts into a step.

        Loads the form from a .ui file and initializes the list of groups to
        make the selection in.  The button is associated with a keyboard
        shortcut and with a function performing the main action.
        """
        # App.Console.PrintMessage('init\n')
        self.form = Gui.PySideUic.loadUi(
            os.path.join(ICONPATH, 'StepSelectDialog.ui'))
        for obj in findObjectsByType(Layer):
            self.form.groupList.addItem(obj.Label)
        button = self.form.addToStep
        button.clicked.connect(self.on_addToStep_clicked)
        button.setShortcut(QtGui.QKeySequence(Qt.Key_S, Qt.Key_S))
        layerContainer = self.getLayerContainer()
        if layerContainer:
            self.visibilityLayers = layerContainer.ViewObject.Visibility
        else:
            self.visibilityLayers = None
        layerContainer.ViewObject.Visibility = False

    def closeDialog(self):
        # App.Console.PrintMessage('closeDialog\n')
        layerContainer = self.getLayerContainer()
        if self.visibilityLayers:
            layerContainer.ViewObject.Visibility = self.visibilityLayers
        Gui.Control.closeDialog()

    def accept(self):
        self.closeDialog()

    def reject(self):
        self.closeDialog()

    def on_addToStep_clicked(self):
        """
        Add selected items in the GUI to a step.

        Steps are represented by LinkGroups.  We first check whether one step
        is selected and whether that is a LinkGroup.  If so, we create a link
        in the selected group for each item in the GUI selection and we make
        sure that these items are not visible anymore, this as a convenience to
        pick objects that are related to this step.
        """

        doc = App.ActiveDocument
        # c = App.Console
        selectedGroups = self.form.groupList.selectedItems()
        if len(selectedGroups) != 1:
            # c.PrintMessage('no selection')
            return
        groupLabel = selectedGroups[0].text()
        # c.PrintMessage(f'the groupLabel: {groupLabel}\n')
        stepGroups = list(filter(lambda obj: obj.Label == groupLabel,
                                 findObjectsByType(Layer)))
        if len(stepGroups) != 1:
            # c.PrintMessage(f'the linkGroups: {linkGroups}\n')
            return
        group = stepGroups[0]
        # c.PrintMessage(f'the group: {group.Label}\n')
        for i in Gui.Selection.getCompleteSelection():
            # c.PrintMessage(f'making a link: {i.Label}\n')
            obj = i.Object
            link = doc.addObject('App::Link', 'Link')
            link.Label = obj.Label
            link.setLink(obj)
            if obj.Parents:
                parent, subobject = obj.Parents[0]
                placement = \
                    parent.getSubObject(subobject,
                                        retType=ReturnType.Placement.value)
            else:
                placement = \
                    obj.getSubObject('', retType=ReturnType.Placement.value)
                # isn't this simply obj.Placement?
            link.Placement = placement
            link.adjustRelativeLinks(group)
            group.ViewObject.dropObject(link, None, '', [])

            # c.PrintMessage(f'setting {i} to invisble\n')
            obj.Visibility = False
            # c.PrintMessage(f'setting {link} to invisble\n')
            # link.Visibility = False
            # doc.recompute()
            # Gui.Selection.clearSelection()
            # Gui.Selection.addSelection(link)
            # Gui.Selection.setVisible(False)
            # Gui.Selection.clearSelection()


class StepSelect(object):
    """
    A command to initiate selecting items for a step
    """

    command_name = 'OSHAutoDoc_StepSelect'

    def IsActive(self):
        """
        Whether this command should be active or not.
        """
        return App.ActiveDocument is not None

    def GetResources(self):
        """
        Get resources for this command.
        """
        return {'Pixmap': os.path.join(ICONPATH, 'select-for-step.svg'),
                'MenuText': 'Select step',
                'ToolTip': 'Select a step for selection'}

    def Activated(self):
        """
        Show the StepSelectPanel as a Dialog on activating this command.
        """
        Gui.Control.showDialog(StepSelectPanel())


# Add the command to the system
Gui.addCommand(StepSelect.command_name, StepSelect())
