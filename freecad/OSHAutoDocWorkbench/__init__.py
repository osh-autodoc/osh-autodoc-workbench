# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

from .version import __version__

# The directory for all the icons
ICONPATH = os.path.join(os.path.dirname(__file__), "resources")

# reporting that the workbench is being loaded
print(f"OSHAutoDoc workbench (v{__version__}) loaded")
