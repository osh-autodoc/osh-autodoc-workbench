# SPDX-FileCopyrightText: 2022 Pieter Hijma <pieter@hiww.de>
# SPDX-FileCopyrightText: 2022 J.C. Mariscal-Melgar <jc@hsu-hh.de>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from setuptools import setup
import os
from freecad.OSHAutoDocWorkbench.version import __version__

version_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                            "freecad", "OSHAutoDocWorkbench", "version.py")
with open(version_path) as fp:
    exec(fp.read())

setup(name='freecad.OSHAutoDocWorkbench',
      version=str(__version__),
      packages=['freecad',
                'freecad.OSHAutoDocWorkbench'],
      maintainer="Pieter Hijma, J.C. Mariscal-Melgar",
      maintainer_email="pieter@hiww.de, jc@hsu-hh.de",
      url="https://codeberg.org/osh-autodoc/osh-autodoc-workbench",
      description="FreeCAD workbench for automated documentation for Open Source Hardware",
      install_requires=[],
      include_package_data=True)
